#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <QString>
#include <QDebug>

#include "optional.h"
#include "museum.h"
#include "csv.h"
#include "storage.h"
#include "idgenerator.h"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
    vector<Museum> museums_;

    static Museum rowToMuseum(const CsvRow & row);
    static CsvRow MuseumToRow(const Museum & mus);

    int getSizeFromFile(string file_name);

public:
    CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

    const string dir_name_;

    bool load();
    bool save();
    void clearIdFile();
    void deleteAll();
    int  size();

    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
};
