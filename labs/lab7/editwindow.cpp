#include "editwindow.h"
#include "ui_editwindow.h"

EditWindow::EditWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditWindow)
{
    ui->setupUi(this);
}

EditWindow::~EditWindow()
{
    delete ui;
}

void EditWindow::on_OK_bt_clicked()
{
    Museum * museum = new Museum();
    museum->city_ = ui->NewCity_lineEdit->text().toStdString();
    museum->name_ = ui->NewName_lineEdit_2->text().toStdString();
    museum->year_ = ui->NewYear_spinbox->value();  //NewYear_lineEdit_3->text().toInt();
    emit sendUpdMuseum(museum);

    EditWindow::close();
}

void EditWindow::on_Cancel_bt_clicked()
{
    EditWindow::close();
}

void EditWindow::receiveMuseumToEdit(QListWidgetItem * item)
{
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    ui->NewName_lineEdit_2->setText(QString::fromStdString(mus.name_));
    ui->NewCity_lineEdit->setText(QString::fromStdString(mus.city_));
    ui->NewYear_spinbox->setValue(mus.year_);// ->setText(QString::fromStdString(to_string(mus.year_)));
}
