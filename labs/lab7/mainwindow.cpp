#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::on_pushButton_1_clicked()
//{
//    QFileDialog dialog(this);
//    dialog.setFileMode(QFileDialog::Directory);
//    QString current_dir = QDir::currentPath();
//    QString default_name = "new_storage";
//    QString folder_path = dialog.getSaveFileName(
//        this,
//        "Select New Storage Folder",
//        current_dir + "/" + default_name + ".csv",
//        "Folders");

//    qDebug() << "Selected directory: " << folder_path;

//    if (folder_path.size() == 0)
//        return;

//    if (storage_ != nullptr)
//    {
//        delete storage_;
//        storage_ = nullptr;

//        ui->listWidget->clear();
//        idGenerator::reset();

//        ui->edit_bt->setEnabled(false);
//        ui->pushButton_5->setEnabled(false);
//        ui->namelabel->setText("-");
//        ui->yearlabel->setText("-");
//        ui->locationlabel->setText("-");
//    }

//    ui->Year_label->setEnabled(true);
//    ui->pushButton_4->setEnabled(true);
//    ui->listWidget->setEnabled(true);
//    ui->Name_label->setEnabled(true);
//    ui->Location_label->setEnabled(true);
//    ui->namelabel->setEnabled(true);
//    ui->locationlabel->setEnabled(true);
//    ui->yearlabel->setEnabled(true);
//    ui->SelectedMus_label->setEnabled(true);

//    file_name = folder_path;

//    CsvStorage * csv = new CsvStorage(folder_path.toStdString());
//    storage_ = csv;
//    storage_->save();
//}

//void MainWindow::on_pushButton_2_clicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(
//                this,              // parent
//                "Dialog Caption",  // caption
//                "",                // directory to start with
//                "CSV (*.csv);;All Files (*)");  // file name filter

//    fileName = "/home/dima/projects/labs/2_семестр/lab_test/a.csv";
//    qDebug() << fileName;

//    string name = fileName.toStdString();
//    const char * namec = name.c_str();
//    char c_name[100];
//    int num_of_slash = 0;
//    for (; *namec != '\0'; namec += 1)
//    {
//        if (*namec == '/')
//            num_of_slash += 1;
//    }
//    namec = name.c_str();
//    for (int i = 0;; i += 1)
//    {
//        c_name[i] = *namec;
//        if (*namec == '/')
//            num_of_slash -= 1;
//        if (num_of_slash == 0)
//        {
//            c_name[i + 1] = '\0';
//            break;
//        }
//        namec += 1;
//    }

//    fileName = c_name;

//    if (fileName.size() == 0)
//        return;

//    qDebug() << "Path:" << fileName;
//    QDir dir(fileName);
//    if (!dir.exists())
//    {
//        qDebug() << "Wrong path:" << fileName;
//        dir.mkpath(".");
//        fileName = fileName + "new_storage.csv";
//        ofstream stor;
//        stor.open(fileName.toStdString(), ios::out);
//        stor.close();
//    }
//    else
//        fileName = QString::fromStdString(name);

//    if (storage_ != nullptr)
//    {
//        delete storage_;
//        storage_ = nullptr;

//        ui->listWidget->clear();
//        idGenerator::reset();
//    }

//    file_name = fileName;

//    CsvStorage * csv = new CsvStorage(fileName.toStdString());
//    storage_ = csv;

//    if(!storage_->load())
//    {
//        cerr << "Can't load storage: " << fileName.toStdString();
//        cerr << "This file doesn't exist" << endl;
//        return;
//    }

//    vector<Museum> museums = storage_->getAllMuseums();

//    for (size_t i = 0; i < museums.size(); i++)
//    {
//        cout << museums.at(i).name_ << endl;

//        QListWidget * ListWidget = ui->listWidget;
//        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

//        QVariant qVariant;
//        qVariant.setValue(museums.at(i));

//        QString name = QString::fromStdString(museums.at(i).name_);
//        qMuseumListItem->setText(name);
//        qMuseumListItem->setData(Qt::UserRole, qVariant);

//        ListWidget->addItem(qMuseumListItem);
//    }

//    if(!storage_->save())
//    {
//        qDebug() << "Can't save in on_bt_2_clicked in mainwindow.cpp" << endl;
//    }
//    else
//        qDebug() << "Saved success in on_bt_2_clicked in mainwindow.cpp" << endl;

//    ui->listWidget->setEnabled(true);
//    ui->pushButton_4->setEnabled(true);
//    ui->Name_label->setEnabled(true);
//    ui->Location_label->setEnabled(true);
//    ui->Year_label->setEnabled(true);
//    ui->namelabel->setEnabled(true);
//    ui->locationlabel->setEnabled(true);
//    ui->yearlabel->setEnabled(true);
//    ui->SelectedMus_label->setEnabled(true);
//    ui->pushButton_5->setEnabled(false);
//    ui->edit_bt->setEnabled(false);
//}

void MainWindow::on_pushButton_4_clicked() //add
{
    add_window = new AddWindow(this);
    add_window->show();

    connect(add_window, SIGNAL(sendMuseum(Museum*)), this, SLOT(receiveMuseum(Museum*)));
}

void MainWindow::on_pushButton_5_clicked() //delete
{
    vector<Museum> museum = storage_->getAllMuseums();
    if (museum.empty())
        return;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
        qDebug() << "Yes was clicked";
    else
    {
        qDebug() << "Yes was *not* clicked";
        return;
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    if (items.at(0) == NULL)
        return;

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Museum mus = var.value<Museum>();
        int ind = mus.id_;
        qDebug() << "ind: " << ind;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeMuseum(ind))
            qDebug() << "Can't delete";
    }
    qDebug() << file_name << endl;

    if(!storage_->save())
        qDebug() << "Can't save in delete func" << endl;
    else
        qDebug() << "Saved in delete func" << endl;

    museum = storage_->getAllMuseums();

    if (museum.empty())
    {
        ui->pushButton_5->setEnabled(false);
        ui->edit_bt->setEnabled(false);

        ui->namelabel->setText("-");
        ui->locationlabel->setText("-");
        ui->yearlabel->setText("-");
        return;
    }

    items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    ui->namelabel->setText(QString::fromStdString(mus.name_));
    ui->locationlabel->setText(QString::fromStdString(mus.city_));
    ui->yearlabel->setText(QString::fromStdString(to_string(mus.year_)));
}

void MainWindow::on_edit_bt_clicked() //edit
{
    edit_window = new EditWindow(this);
    edit_window->show();

    connect(this, SIGNAL(sendMuseumToEditForm(QListWidgetItem*)), edit_window, SLOT(receiveMuseumToEdit(QListWidgetItem*)));
    connect(edit_window, SIGNAL(sendUpdMuseum(Museum*)), this, SLOT(receiveUpdMuseum(Museum*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendMuseumToEditForm(item));
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem * museum)
{
    ui->pushButton_5->setEnabled(true);
    ui->edit_bt->setEnabled(true);

    QVariant var = museum->data(Qt::UserRole);
    Museum mus = var.value<Museum>();
    qDebug() << mus.id_;

    string year = to_string(mus.year_);
    ui->namelabel->setText(QString::fromStdString(mus.name_));
    ui->locationlabel->setText(QString::fromStdString(mus.city_));
    ui->yearlabel->setText(QString::fromStdString(year));
}

void MainWindow::receiveMuseum(Museum * museum)
{
    storage_->insertMuseum(*museum);
    vector<Museum> museums = storage_->getAllMuseums();

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qMuseumListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(museums.at(museums.size() - 1));

    QString name = QString::fromStdString(museums.at(museums.size() - 1).name_);
    qMuseumListItem->setText(name);
    qMuseumListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qMuseumListItem);
    storage_->save();
}

void MainWindow::receiveUpdMuseum(Museum * museum)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();
    museum->id_ = mus.id_;

    if(!storage_->updateMuseum(*museum))
    {
        qDebug() << "Can't update";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }
    storage_->save();

    QListWidgetItem * qMuseumListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(*museum);

    QString name = QString::fromStdString(museum->name_);
    qMuseumListItem->setText(name);
    qMuseumListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qMuseumListItem);
}

void MainWindow::on_actionCreate_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new_storage";
    QString folder_path = dialog.getSaveFileName(
        this,
        "Select New Storage Folder",
        current_dir + "/" + default_name + ".csv",
        "Folders");

    qDebug() << "Selected directory: " << folder_path;

    if (folder_path.size() == 0)
        return;

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();
        idGenerator::reset();

        ui->edit_bt->setEnabled(false);
        ui->pushButton_5->setEnabled(false);
        ui->namelabel->setText("-");
        ui->yearlabel->setText("-");
        ui->locationlabel->setText("-");
    }

    ui->Year_label->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->listWidget->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);

    file_name = folder_path;

    CsvStorage * csv = new CsvStorage(folder_path.toStdString());
    storage_ = csv;
    storage_->save();
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "",                // directory to start with
                "CSV (*.csv);;All Files (*)");  // file name filter

//    fileName = "/home/dima/projects/labs/2_семестр/lab_test/a.csv";
    qDebug() << fileName;

    string name = fileName.toStdString();
    const char * namec = name.c_str();
    char c_name[100];
    int num_of_slash = 0;
    for (; *namec != '\0'; namec += 1)
    {
        if (*namec == '/')
            num_of_slash += 1;
    }
    namec = name.c_str();
    for (int i = 0;; i += 1)
    {
        c_name[i] = *namec;
        if (*namec == '/')
            num_of_slash -= 1;
        if (num_of_slash == 0)
        {
            c_name[i + 1] = '\0';
            break;
        }
        namec += 1;
    }

    fileName = c_name;

    if (fileName.size() == 0)
        return;

    qDebug() << "Path:" << fileName;
    QDir dir(fileName);
    if (!dir.exists())
    {
        qDebug() << "Wrong path:" << fileName;
        dir.mkpath(".");
        fileName = fileName + "new_storage.csv";
        ofstream stor;
        stor.open(fileName.toStdString(), ios::out);
        stor.close();
    }
    else
        fileName = QString::fromStdString(name);

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();
        idGenerator::reset();
    }

    file_name = fileName;

    CsvStorage * csv = new CsvStorage(fileName.toStdString());
    storage_ = csv;

    if(!storage_->load())
    {
        cerr << "Can't load storage: " << fileName.toStdString();
        cerr << "This file doesn't exist" << endl;
        return;
    }

    vector<Museum> museums = storage_->getAllMuseums();

    for (size_t i = 0; i < museums.size(); i++)
    {
        cout << museums.at(i).name_ << endl;

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(museums.at(i));

        QString name = QString::fromStdString(museums.at(i).name_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
    }

    if(!storage_->save())
    {
        qDebug() << "Can't save in on_bt_2_clicked in mainwindow.cpp" << endl;
    }
    else
        qDebug() << "Saved success in on_bt_2_clicked in mainwindow.cpp" << endl;

    ui->listWidget->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->Year_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);
    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
}
