#-------------------------------------------------
#
# Project created by QtCreator 2019-05-05T20:06:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab7
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    idgenerator.cpp \
    csv_storage.cpp \
    csv.cpp \
    museum.cpp \
    addwindow.cpp \
    editwindow.cpp

HEADERS += \
        mainwindow.h \
    idgenerator.h \
    storage.h \
    optional.h \
    museum.h \
    csv_storage.h \
    csv.h \
    addwindow.h \
    editwindow.h

FORMS += \
        mainwindow.ui \
    addwindow.ui \
    editwindow.ui

DISTFILES += \
    data/csv/museums.csv
