#ifndef IDGENERATOR_H
#define IDGENERATOR_H

class idGenerator
{
    static int id_;
public:
    static int id_get();
    static void reset();
};

#endif // IDGENERATOR_H
