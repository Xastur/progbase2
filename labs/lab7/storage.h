#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "museum.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
public:
    virtual ~Storage() = default;

    virtual bool load() = 0;
    virtual bool save() = 0;
    virtual void clearIdFile() = 0;
    virtual void deleteAll() = 0;
    virtual int  size() = 0;

    virtual vector<Museum> getAllMuseums(void) = 0;
    virtual optional<Museum> getMuseumById(int museum_id) = 0;
    virtual bool updateMuseum(const Museum & museum) = 0;
    virtual bool removeMuseum(int museum_id) = 0;
    virtual int insertMuseum(const Museum & museum) = 0;
};
