#include "list.h"
#include "csv.h"

static void   List_memEx(List *self); 

List *List_alloc()
{
    List *list;
    if ((list = malloc(sizeof(List))) == NULL)
    {
        printf("ERROR. CAN NOT ALLOCATE\n");
        exit(1);
    }
    List_init(list);
    return list;
}

void List_init(List *self)
{
    self->capacity = 8;
    if ((self->items = malloc(sizeof(void *) * self->capacity)) == NULL)
    {
        printf("ERROR. CAN NOT ALLOCATE\n");
        exit(1);
    }
    self->size = 0;
}

void List_deinit(List *self)
{
    free(self->items);
}

void List_free(List *self)
{
    List_deinit(self);
    free(self);
}

void *List_get(List *self, int index)
{
    return self->items[index];
}

void List_set(List *self, int index, void *value)
{
    self->items[index] = value;
}

size_t List_size(List *self)
{
    return self->size;
}

void List_insert(List *self, int index, void *value)
{
    if (self->capacity == self->size)
    {
        List_memEx(self);
    }

    for (int i = self->size; i > index; i--)
    {
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;
    self->size += 1;
}

void List_removeAt(List *self, int index)
{
    char *s = List_get(self, index);
    for (int i = index; i < List_size(self) - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    free(s);
    self->size -= 1;
}

void List_add(List *self, void *value)
{
    if (self->size == self->capacity)
    {
        List_memEx(self);
    }
    self->items[self->size++] = value;
}

void List_remove(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
        {
            List_removeAt(self, i);
            return;
        }
    }
}

int List_indexOf(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
            return i;
    }
    return -1;
}

bool List_contains(List *self, void *value)
{
    for (int i = 0; i < List_size(self); i++)
    {
        if (self->items[i] == value)
            return true;
    }
    return false;
}

bool List_isEmpty(List *self)
{
    if (self->size == 0)
        return true;

    return false;
}

void *StrOnHeap(void *str)
{
    char *s = malloc(strlen(str) + 1);
    strcpy(s, str);
    return s;
}

void List_printString(List *self, int ind, FILE * fp)
{
    fprintf(fp, "%s", (char *)List_get(self, ind));
}

void List_print(List *self, FILE * fp)
{
    for (int i = 0; i < List_size(self); i++)
    {
        List_printString(self, i, fp);
        if (i < List_size(self) - 1)
            fprintf(fp, ",");
    }
    fprintf(fp, "\n");
}

static void List_memEx(List *self)
{
    self->capacity *= 2;
    void **new_val = realloc(self->items, sizeof(char *) * self->capacity);
    if (new_val == NULL)
    {
        printf("ERROR. CAN NOT REALLOCATE\n");
        List_free(self);
        exit(1);
    }
    self->items = new_val;
}
