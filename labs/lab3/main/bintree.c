#include "bintree.h"
#include "bstree.h"

void      BinTree_init     (BinTree * self, StrStrMap value)
{
    self->left = NULL;
    self->right = NULL;
    self->map = value;
}

void      BinTree_deinit   (BinTree * self)
{
    //
}

BinTree * BinTree_alloc (StrStrMap value)
{
    BinTree * self = malloc(sizeof(BinTree));
    BinTree_init(self, value);
    return self;
}

void      BinTree_free     (BinTree * self)
{
    BinTree_deinit(self);
    free(self);
}