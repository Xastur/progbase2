#pragma once

#include <stdlib.h> 
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

typedef struct KeyValue KeyValue;
struct KeyValue
{
    const char * key;
    const char * value;
};

typedef struct __KeyValueList KeyValueList;
struct __KeyValueList 
{
   KeyValue  * items;
   size_t capacity;
   size_t size;
};

int          KeyValue_compare      (KeyValue * a, KeyValue * b);
void         KeyValueList_init     (KeyValueList * self);
void         KeyValueList_deinit   (KeyValueList * self);

size_t       KeyValueList_size     (KeyValueList * self);
KeyValue   * KeyValueList_get      (KeyValueList * self, int index);
const char * KeyValue_getKey       (KeyValue * self);
const char * KeyValue_getVal       (KeyValue * self);

void         KeyValueList_set      (KeyValueList * self, int index, KeyValue value);
void         KeyValueList_removeAt (KeyValueList * self, int index);

void         KeyValueList_add      (KeyValueList * self, KeyValue value);  
void         KeyValueList_remove   (KeyValueList * self, KeyValue value);
int          KeyValueList_indexOf  (KeyValueList * self, KeyValue value);  
bool         KeyValueList_contains (KeyValueList * self, KeyValue value);  
 
void         KeyValueList_clear    (KeyValueList * self);
void         KeyValueList_print    (KeyValueList * self, FILE * fp);