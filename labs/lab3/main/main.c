#include "strstrmap.h"
#include "bintree.h"
#include "bstree.h"
#include "kvlist.h"
#include "lab.h"

int main(int argc, char *argv[])
{
    char data_name[20];
    data_name[0] = '\0';

    char out[20];
    out[0] = '\0';

    char city[20];
    city[0] = '\0';

    char str[1000];
    str[0] = '\0';
    int ind = 0;

    bool b = false;
    bool dataHasRead = false;

    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "-n") == 0)
        {
            if (i + 1 == argc)
            {
                printf("option '-n' without value\n");        
                exit(EXIT_FAILURE);
            }

            if (strcmp(argv[i + 1], "-o") == 0 || strcmp(argv[i + 1], "-b") == 0)
            { 
                printf("option '-n' without value\n"); 
                exit(EXIT_FAILURE);       
            }

            strcpy(city, argv[i + 1]);
            printf("%s\n", argv[i]);
            i += 1;
        }
        else if (strcmp(argv[i], "-o") == 0)
        {
            if (i + 1 == argc)
            {
                printf("option '-o' without value\n");        
                exit(EXIT_FAILURE);
            }

            if (strcmp(argv[i + 1], "-n") == 0 || strcmp(argv[i + 1], "-b") == 0)
            {
                printf("option '-o' without value\n");
                exit(EXIT_FAILURE);
            }

            strcpy(out, argv[i + 1]);
            printf("%s\n", argv[i]);
            i += 1;
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            b = true;
        }
        else 
        {
            if (dataHasRead == false && i > 0)
            {
                dataHasRead = true;
                strcpy(data_name, argv[i]);
            }
            else if (i > 0)
            {
                printf("unknown value: %s\n", argv[i]);
            }
        }
        printf("%s\n", argv[i]);
    }
    printf("\n");

    if (strlen(out) > 0)
    {
        printf("OUT: %s\n", out);
    }

    BSTree * new_tree = NULL;

    if (dataHasRead)
    {
        printf("Data name: %s\n", data_name);

        FILE *p = NULL;
        p = fopen(data_name, "r");
        if (p == NULL)
        {
            fprintf(stderr, "CAN NOT OPEN %s\n", data_name);
            exit(EXIT_FAILURE);
        }
        char c = getc(p);
        while (c != EOF)
        {
            str[ind++] = c;
            c = getc(p);
        }
        str[ind] = '\0';
        fclose(p);
        //
        new_tree =  BSTree_createTreeFromString(str);
        printBinTree(new_tree->root, stdout);
        printf("\n");
    }

    BSTree * tree = NULL;
    List * list = NULL;

    if (b)
    {
        list = List_alloc();

        StrStrMap * map1 = createMuseumMap(00002, "name1", "city1", 1234, "x visitors");
        StrStrMap * map2 = createMuseumMap(00003, "name2", "city2", 1345, "y visitors");
        StrStrMap * map3 = createMuseumMap(00001, "name3", "city3", 1456, "z visitors");

        List_add(list, map1);
        List_add(list, map2);
        List_add(list, map3);

        tree = BSTree_createTreeFromList(list);
        printBinTree(tree->root, stdout);

        List_free(list);
    }

    List * new_list = NULL;

    if (strlen(city) > 0 && dataHasRead)
    {
        BSTree_updTree(new_tree, list, city);
        printf("\nUpdated tree:\n");
        printBinTree(new_tree->root, stdout);
    }

    printf("\n"); 

    if (tree != NULL)
    {
        BSTree_free(tree);
    }

    List * table = NULL;
    if (strlen(out) > 0)
    {
        table = List_alloc();

        FILE * pout = fopen(out, "w");
        if (pout == NULL)
        {
            fprintf(stdout, "Can not open %s\n", out);
        }
        BSTree_fillTableFromTree(new_tree, table);
        Csv_printTable(table, pout);
        fclose(pout);
    }

    if (new_tree != NULL)
    {
        BSTree_free(new_tree);
    }

    if (new_list != NULL)
    {
        List_free(new_list);
    }

    if (table != NULL)
    {
        Csv_clearTable(table);    
    }

    return 0;
}