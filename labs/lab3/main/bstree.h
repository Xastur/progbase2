#pragma once 

#include "bintree.h"
#include "strstrmap.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct __BSTree BSTree;
struct __BSTree {
    BinTree * root;
    size_t size;   
};

void        BSTree_init         (BSTree * self, StrStrMap value);
void        BSTree_deinit       (BSTree * self);
   
BSTree *    BSTree_alloc        (StrStrMap * value);
void        BSTree_free         (BSTree * self);
  
size_t      BSTree_size         (BSTree * self);  // number of stored values
   
int         getKey              (StrStrMap value);  // there should be a function to get key from value
   
void        BSTree_insert       (BSTree * self, StrStrMap * value);  // add unique
bool        BSTree_lookup       (BSTree * self, int key);  // check for value with a key
StrStrMap * BSTree_search       (BSTree * self, int key);  // get the value for a key
void        BSTree_delete       (BSTree * self, int key);  // delete the value for a key
void        BSTree_clear        (BinTree * self);
  
void        BSTree_preOrdPrint  (BSTree * self, FILE * fp);
void        BSTree_postOrdPrint (BSTree * self, FILE * fp);
void        BSTree_inOrdPrint   (BSTree * self, FILE * fp);
     
void        printBinTree        (BinTree * root, FILE * fp);