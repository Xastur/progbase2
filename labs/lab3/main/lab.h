#pragma once

#include "bstree.h"
#include <csv.h>

struct Museum
{
    char *name;
    char *location;
    int  establ;
    char *visitors; 
};

void   List_printStrStrMap (List * items, FILE * fp); 
void   List_strStrMapFree (List *self); 
void   List_strStrMapDeinit (List * self); 

BSTree  * BSTree_createTreeFromString (const char * csvString);
BSTree  * BSTree_createTreeFromList (List * list);

void      BSTree_updTree      (BSTree * self, List * list, char * n);
List    * List_createListFromBST (BSTree * self);
void      BSTree_fillTableFromTree (BSTree * self, List * table);

List *    createMuseumListFromTable(List * csvTable); 

void   processItems2(List * items, char * n); 

List * List_toTable(List * self); 
void   List_valueFromListToStr(List * self, char * str, int n); 