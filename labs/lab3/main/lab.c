#include "lab.h"

void   List_printStrStrMap (List * items, FILE * fp)
{
    for (int i = 0; i < List_size(items); i++)
    {
        StrStrMap * m = List_get(items, i);
        StrStrMap_print(m, fp);
    }
}

void List_strStrMapDeinit (List *self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        StrStrMap * map = List_get(self, i);
        StrStrMap_free(map);
    }
    List_deinit(self);
}

void   List_strStrMapFree (List *self)
{
    List_strStrMapDeinit(self);
    free(self);
}

void        BSTree_updTree      (BSTree * self, List * list, char * n)
{
    List * l = NULL;
    l = List_createListFromBST(self);
    int key;
    if (List_size(l) == 0 || self == NULL)
    {
        return;
    }
    for (int i = 0; i < List_size(l); i++)
    {
        StrStrMap * m = List_get(l, i);
        key = atoi(KeyValue_getVal(StrStrMap_getWithInd(m, 0)));
        if (strcmp(KeyValue_getVal(StrStrMap_getWithInd(m, 2)), n) != 0)
        {
            BSTree_delete(self, key);
        }
    }

    List_free(l);
}

static void create (List * list, BinTree * self)
{
    if (self == NULL)
        return;
    create(list, self->left);
    create(list, self->right);
    List_add(list, &self->map);
}

List      * List_createListFromBST (BSTree * self)
{
    if (self == NULL)
        return NULL;

    List * list = List_alloc();
    create(list, self->root);
    return list;
}

static void fill (BinTree * self, char * str)
{
    if (self == NULL)
        return;

    fill(self->left, str);
    fill(self->right, str);
    StrStrMap_mapToStr(&self->map, str);   
}

void        BSTree_fillTableFromTree (BSTree * self, List * table)
{
    if (self == NULL)
        return;

    char str[5000];
    str[0] = '\0';
    fill(self->root, str);
    Csv_fillTableFromString(table, str);
}

BSTree    * BSTree_createTreeFromString (const char * csvString)
{
    char buf[100];
    int bufX = 0;
    int iter = 0;
    StrStrMap * map = NULL;
    BSTree * self = NULL;
    while (*csvString)
    {
        if (*csvString == '"')
        {
            csvString += 1;
            while(1)
            {
                if (*csvString == '"' && *(csvString + 1) == '"')
                {
                    buf[bufX++] = '"';
                    csvString += 1;
                }
                else if ((*csvString == '"' && *(csvString + 1) == ',') || (*csvString == '"' && *(csvString + 2) == '\n') || (*csvString == '"' && *(csvString + 2) == '\0'))
                {
                    buf[bufX] = '\0';
                    if (iter == 0)
                    {
                    }
                    if (iter == 1)
                    {
                        StrStrMap_add(map, "name", StrOnHeap(&buf[0]));
                    }
                    if (iter == 2)
                    {
                        StrStrMap_add(map, "location", StrOnHeap(&buf[0]));
                    }
                    if (iter == 3)
                    {
                    }
                    if (iter == 4)
                    {
                        StrStrMap_add(map, "visitors", StrOnHeap(&buf[0]));
                        if (self == NULL)
                            self = BSTree_alloc(map);
                        else 
                            BSTree_insert(self, map);

                        map = NULL;
                        iter = -2;
                    }
                    bufX = 0;
                    csvString += 1;
                    iter += 1;
                    break;
                }
                else 
                {
                    buf[bufX++] = *csvString;
                }
                csvString += 1;
            }
        }
        else if (*csvString == ',' || *csvString == '\n')
        {
            buf[bufX] = '\0';
            if (iter == 0)
            {
                map = StrStrMap_alloc();
                StrStrMap_add(map, "id", StrOnHeap(&buf[0]));
            }
            if (iter == 1)
            {
                StrStrMap_add(map, "name", StrOnHeap(&buf[0]));
            }
            if (iter == 2)
            {
                StrStrMap_add(map, "location", StrOnHeap(&buf[0]));
            }
            if (iter == 3)
            {
                StrStrMap_add(map, "establ", StrOnHeap(&buf[0]));
            }
            if (iter == 4)
            {
                buf[--bufX] = '\0';
                StrStrMap_add(map, "visitors", StrOnHeap(&buf[0]));
                if (self == NULL)
                    self = BSTree_alloc(map);
                else 
                    BSTree_insert(self, map);

                map = NULL;
                iter = -1;
            }
            iter++;
            bufX = 0;
        }
        else if (*(csvString + 1) == '\0')
        {
            buf[bufX++] = *csvString;
            buf[bufX] = '\0';
            StrStrMap_add(map, "visitors", StrOnHeap(&buf[0]));
            BSTree_insert(self, map);
        }
        else 
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }

    return self;
}

BSTree    * BSTree_createTreeFromList (List * list)
{
    if (List_size(list) == 0)
    {
        fprintf(stderr, "List is empty\n");
        exit(EXIT_FAILURE);
    }
    BSTree * self = NULL;

    for (int i = 0; i < List_size(list); i++)
    {
        void * p = List_get(list, i);
        if (self == NULL)
            self = BSTree_alloc(p);
        else 
            BSTree_insert(self, p);
    }
    return self;
}

List * createMuseumListFromTable(List * csvTable)
{
    List *new_list = List_alloc();
    struct Museum *mus = NULL;
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *list = List_get(csvTable, i);
        mus = malloc(sizeof(struct Museum));
        mus->name = (char*)List_get(list, 0);
        mus->location = List_get(list, 1);
        char *c = (char *)List_get(list, 2);
        mus->establ = atoi(c);
        mus->visitors = (char *)List_get(list, 3);
        List_add(new_list, mus);
    }
    return new_list;
}

void   processItems2(List * items, char * n)
{
    for (int i = 0; i < List_size(items); i += 1)
    {
        struct Museum *mus = List_get(items, i);
        if (strcmp(mus->location, n) == 0)
        {
        }
        else 
        {
            List *l = items->items[i];
            for (int j = i; j < List_size(items) - 1; j++)
            {
                items->items[j] = items->items[j + 1];
            }
            free(l);
            items->size--;
            i--;
        }
    }
}

List * List_toTable(List * self)
{
    char str[1000];
    List_valueFromListToStr(self, str, 1000);
    for (int i = 0; i < List_size(self); i++)
    {
        List *l = List_get(self, i);
        free(l);
    }
    List_deinit(self);
    List_init(self);
    Csv_fillTableFromString(self, str);
    return self;
}

void   List_valueFromListToStr(List * self, char * str, int n)
{
    str[0] = '\0';
    for (int i = 0; i < self->size; i++)
    {
        struct Museum *mus = self->items[i];
        strcat(str, mus->name);
        strcat(str, ",");
        strcat(str, mus->location);
        strcat(str, ",");
        char  establ[10];
        sprintf(establ, "%d", mus->establ);
        strcat(str, establ);
        strcat(str, ",");
        strcat(str, mus->name);
        strcat(str, "\n");
    }
}