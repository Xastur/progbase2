#pragma once 

#include "kvlist.h"
#include "strstrmap.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct __BinTree BinTree;
struct __BinTree
{
   StrStrMap map;
   BinTree * left; 
   BinTree * right;
};

void      BinTree_init     (BinTree * self, StrStrMap value);
void      BinTree_deinit   (BinTree * self);

BinTree * BinTree_alloc    (StrStrMap value);
void      BinTree_free     (BinTree * self);