#include "bstree.h"
#include "strstrmap.h"

BSTree *    BSTree_alloc        (StrStrMap * value)
{
    BSTree * self = malloc(sizeof(BSTree));
    BSTree_init(self, *value);
    free(value);
    return self;
}

void        BSTree_init         (BSTree * self, StrStrMap value)
{
    self->root = BinTree_alloc(value);
    self->size = 0;
}

void     BSTree_deinit          (BSTree * self)
{
    BSTree_clear(self->root);
}

void     BSTree_clear           (BinTree * self)
{
    if (self == NULL)
    {
        return;
    }
    BSTree_clear(self->left);
    BSTree_clear(self->right);
    StrStrMap_free(&self->map);
}

void     BSTree_free            (BSTree * self)
{
    BSTree_deinit(self);
    free(self);
}

size_t   BSTree_size            (BSTree * self)
{
    return self->size;
}

int         getKey              (StrStrMap value)
{
    KeyValue * kv = KeyValueList_get(&value.list, 0);
    return atoi(KeyValue_getVal(kv));
}

static void insert           (BinTree * self, BinTree * new_node)
{
    if (getKey(self->map) == getKey(new_node->map))
    {
        fprintf(stderr, "same value: %d\n", getKey(self->map));
        exit(EXIT_FAILURE);
    }
    if (getKey(new_node->map) < getKey(self->map))
    {
        if (self->left == NULL)
        {
            self->left = new_node;
            return;
        }
        insert(self->left, new_node);
    }
    else if (getKey(new_node->map) > getKey(self->map))
    {
        if (self->right == NULL)
        {
            self->right = new_node;
            return;
        }
        insert(self->right, new_node);
    }
}

void        BSTree_insert       (BSTree * self, StrStrMap * value)  // add unique
{
    BinTree * new_node = BinTree_alloc(*value);
    free(value);

    if (self == NULL)
    {
        self->root = new_node;
    }
    else 
    {
        insert(self->root, new_node);
    }
}

static bool lookup           (BinTree * self, int key)
{
    if (self == NULL)
        return false; 

    if (getKey(self->map) < key)
    {
        return lookup(self->right, key);
    }
    if (getKey(self->map) > key)
    {
        return lookup(self->left, key);
    }
    return true;
}

bool     BSTree_lookup       (BSTree * self, int key)
{
    if (self == NULL)
        return false; 

    else 
    {
        return lookup(self->root, key);
    }
}

static StrStrMap * search     (BinTree * self, int key)
{
    if (self == NULL)
        return NULL;

    if (getKey(self->map) < key)
    {
        return search(self->right, key);
    }
    if (getKey(self->map) > key)
    {
        return search(self->left, key);
    }
    return &self->map;
}

StrStrMap * BSTree_search       (BSTree * self, int key)  // get the value for a key
{
    if (self == NULL)
        return NULL;
    
    else 
    {
        return search(self->root, key);
    }
}

BinTree * minNodeSearch             (BinTree * self)
{
    if (self == NULL)
        return NULL;

    if (self->left == NULL)
        return self;

    return minNodeSearch(self->left);
}

static StrStrMap * delete       (BinTree * self, int key, BinTree * parent);

static void BSTree_modDelete(BinTree * self, BinTree * parent)
{
    if (self->left == NULL && self->right == NULL)
    {
        if (parent->left == self)
            parent->left = NULL;
        else 
            parent->right = NULL;
    }
    else if (self->left == NULL || self->right == NULL)
    {
        if (self->left != NULL)
        {
            if (parent->left == self)
                parent->left = self->left;
            else 
                parent->right = self->left;
        }
        else 
        {
            if (parent->left == self)
                parent->left = self->right;
            else 
                parent->right = self->right;   
        }
    }
    else
    {
        BinTree *minNode = minNodeSearch(self->right);
        KeyValue * kv = StrStrMap_getWithInd(&minNode->map, 0);
        int minKey = atoi(KeyValue_getKey(kv));
        StrStrMap mp = *search(self, minKey);
        BinTree *newNode = BinTree_alloc(mp);
        delete(self->right, minKey, self);
        if (parent->left == self)
            parent->left = newNode;
        else
            parent->right = newNode;
        newNode->left = self->left;
        newNode->right = self->right;
    }
}

static StrStrMap * delete       (BinTree * self, int key, BinTree * parent)
{
    if (self == NULL)
        return NULL; 

    if (getKey(self->map) < key)
    {
        return delete(self->right, key, self);
    }
    if (getKey(self->map) > key)
    {
        return delete(self->left, key, self);
    }
    BSTree_modDelete(self, parent);
    StrStrMap * old = &self->map;
    StrStrMap_free(old);
    return old;
}

void BSTree_delete       (BSTree * self, int key)  // delete the value for a key
{
    char arr[10];
    sprintf(arr, "%d", key);
    KeyValue * kv = StrStrMap_getWithInd(&self->root->map, 0);
    if (self == NULL || strcmp(arr, KeyValue_getVal(kv)) == 0)
        return;

    BinTree fakenode;
    fakenode.left = self->root->left;
    delete(self->root, key, &fakenode);    
}

void static preOrdPrint     (BinTree * self, FILE * fp)
{
    if (self == NULL)
        return;
    StrStrMap_print(&self->map, fp);
    preOrdPrint(self->left, fp);
    preOrdPrint(self->right, fp);
}

void     BSTree_preOrdPrint  (BSTree * self, FILE * fp)
{
    preOrdPrint(self->root, fp);
}

static void postOrdPrint     (BinTree * self, FILE * fp)
{
    if (self == NULL)
    return;
    postOrdPrint(self->left, fp);
    postOrdPrint(self->right, fp);
    StrStrMap_print(&self->map, fp);
}

void     BSTree_postOrdPrint (BSTree * self, FILE * fp)
{
    postOrdPrint(self->root, fp);
}

static void inOrdPrint       (BinTree * self, FILE * fp)
{
    if (self == NULL)
        return;
    inOrdPrint(self->left, fp);
    StrStrMap_print(&self->map, fp);
    inOrdPrint(self->right, fp);
}

void     BSTree_inOrdPrint   (BSTree * self, FILE * fp)
{
    if (self == NULL)
        return;
    inOrdPrint(self->root, fp);
}

static void printValueOnLevel(BinTree * node, char pos, int lvl, FILE * fp)
{
   for (int i = 0; i < lvl; i++) {
       fprintf(fp, "....");
   }
   fprintf(fp, "%c: ", pos);

   if (node == NULL) {
       fprintf(fp, "(null)\n");
   } else {
       StrStrMap_print(&node->map, fp);
   }
}

static void printNode(BinTree * node, char pos, int lvl, FILE * fp);

void printBinTree(BinTree * root, FILE * fp)
{
    printNode(root, '+', 0, fp); 
}

static void printNode(BinTree * node, char pos, int lvl, FILE * fp)
{  
   bool hasChild = node != NULL && (node->left != NULL || node->right != NULL);
   if (hasChild) printNode(node->right, 'R', lvl + 1, fp);
   printValueOnLevel(node, pos, lvl, fp);
   if (hasChild) printNode(node->left,  'L', lvl + 1, fp);
}