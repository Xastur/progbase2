#include "strstrmap.h"

static char       * String_allocCopy       (const char * value);
static char       * String_allocFromSize_t (size_t value);
static char       * String_allocFromInt    (int value);

StrStrMap *StrStrMap_alloc()
{
    StrStrMap *map = malloc(sizeof(StrStrMap));
    StrStrMap_init(map);
    return map;
}

void StrStrMap_init(StrStrMap *self)
{
    KeyValueList_init(&self->list);
}

void StrStrMap_deinit(StrStrMap *self)
{
    for (int i = 0; i < StrStrMap_size(self); i++)
    {
        KeyValue * kv = KeyValueList_get(&self->list, i);
        void * p = (void *)KeyValue_getVal(kv);
        free(p);
    }
    KeyValueList_deinit(&self->list);
}

void StrStrMap_free(StrStrMap *self)
{
    StrStrMap_deinit(self);
    free(self);
}

size_t StrStrMap_size(StrStrMap *self)
{
    return KeyValueList_size(&self->list);
}

void StrStrMap_add(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    KeyValueList_add(&self->list, kv);
}

bool StrStrMap_contains(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    return KeyValueList_contains(&self->list, kv);
}

const char *StrStrMap_get(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    int index = KeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue val = *KeyValueList_get(&self->list, index);
    // return val.value;
    return KeyValue_getVal(&val);
}

const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value)
{
    KeyValue kv;
    kv.key = key;
    kv.value = value;
    int index = KeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue old = *KeyValueList_get(&self->list, index);
    KeyValueList_set(&self->list, index, kv);
    // return old.value;
    return KeyValue_getVal(&old);
}

const char *StrStrMap_remove(StrStrMap *self, const char *key)
{
    KeyValue kv;
    kv.key = key;
    int index = KeyValueList_indexOf(&self->list, kv);
    if (index == -1)
    {
        fprintf(stderr, "Error\n");
    }
    KeyValue old = *KeyValueList_get(&self->list, index);
    KeyValueList_remove(&self->list, kv);
    // return old.value;
    return KeyValue_getVal(&old);
}

void StrStrMap_clear(StrStrMap *self)
{
    KeyValueList_clear(&self->list);
}

StrStrMap *createMuseumMap(int id, const char *name, const char *location, size_t establ, const char *visitors)
{
    char *idValue = String_allocFromInt(id);
    char *nameValue = String_allocCopy(name);
    char *locationValue = String_allocCopy(location);
    char *estValue = String_allocFromSize_t(establ);
    char *visitorsValue = String_allocCopy(visitors);

    StrStrMap *map = StrStrMap_alloc();
    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "name", nameValue);
    StrStrMap_add(map, "location", locationValue);
    StrStrMap_add(map, "establ", estValue);
    StrStrMap_add(map, "visitors", visitorsValue);

    assert(StrStrMap_contains(map, "id"));
    assert(StrStrMap_contains(map, "name"));
    assert(StrStrMap_contains(map, "location"));
    assert(StrStrMap_contains(map, "establ"));
    assert(StrStrMap_contains(map, "visitors"));

    return map;
}

static char *String_allocCopy(const char *value)
{
    char *strOnHeap = malloc(sizeof(char) * (strlen(value)) + 1);
    strcpy(strOnHeap, value);
    return strOnHeap;
}

static char *String_allocFromSize_t(size_t value)
{
    char size_tToString[10];
    sprintf(size_tToString, "%li", value);
    char *strOnHeap = malloc(sizeof(char) * strlen(size_tToString) + 1);
    strcpy(strOnHeap, size_tToString);
    return strOnHeap;
}

static char *String_allocFromInt(int value)
{
    char intToString[10];
    sprintf(intToString, "%i", value);
    char *strOnHeap = malloc(sizeof(char) * strlen(intToString) + 1);
    strcpy(strOnHeap, intToString);
    return strOnHeap;
}

void StrStrMap_printString(StrStrMap *self, int ind, FILE * fp)
{
    KeyValue * kv1 = KeyValueList_get(&self->list, ind);
    fprintf(fp, "%s: ", KeyValue_getKey(kv1));
    fprintf(fp, "%s; ", KeyValue_getVal(kv1));

    KeyValue * kv2 = KeyValueList_get(&self->list, ind + 1);
    fprintf(fp, "%s: ", KeyValue_getKey(kv2));
    fprintf(fp, "%s; ", KeyValue_getVal(kv2));

    KeyValue * kv3 = KeyValueList_get(&self->list, ind + 2);
    fprintf(fp, "%s: ", KeyValue_getKey(kv3));
    fprintf(fp, "%s; ", KeyValue_getVal(kv3));

    KeyValue * kv4 = KeyValueList_get(&self->list, ind + 3);
    fprintf(fp, "%s: ", KeyValue_getKey(kv4));
    fprintf(fp, "%s; ", KeyValue_getVal(kv4));

    KeyValue * kv5 = KeyValueList_get(&self->list, ind + 4);
    fprintf(fp, "%s: ", KeyValue_getKey(kv5));
    fprintf(fp, "%s\n", KeyValue_getVal(kv5));
}

void StrStrMap_print(StrStrMap *self, FILE * fp)
{
    if (self == NULL || StrStrMap_size(self) == 0)
    {
        fprintf(fp, "Map does not exist or is empty\n");
        return;
    }
    for (int i = 0; i < StrStrMap_size(self); i += 5)
    {
        StrStrMap_printString(self, i, fp);
    }
}

static void insertCharIntoArray(char *arr, char c, int ind)
{
    int len = strlen(arr);
    for (int i = len; i > ind; i--)
    {
        arr[i] = arr[i - 1];
    }
    arr[ind] = c;
}

static void prArray(char *buf)
{
    int bufX = 0;
    bool quot = true;

    while (buf[bufX])
    {
        if (quot)
        {
            insertCharIntoArray(buf, '"', 0);
            strcat(buf, "\"");
            bufX += 2;
            quot = false;
        }

        if (buf[bufX] == '"' && buf[bufX + 1] != '\0')
        {
            insertCharIntoArray(buf, '"', bufX);
            bufX += 1;
        }

        bufX += 1;
    }
}

static void clearArr(char *arr)
{
    for (int i = 0; i < 100; i++)
    {
        arr[i] = '\0';
    }
}

void StrStrMap_mapToStr(StrStrMap *self, char *str)
{
    char buf[100];
    int bufX = 0;
    buf[bufX] = '\0';

    if (strlen(str) > 0)
        strcat(str, "\n");

    KeyValue * kv = KeyValueList_get(&self->list, 0);
    strcat(str, KeyValue_getVal(kv));
    strcat(str, ",");

    kv = KeyValueList_get(&self->list, 1);
    strcat(buf, KeyValue_getVal(kv));
    prArray(buf);
    strcat(str, buf);
    buf[0] = '\0';
    strcat(str, ",");
    clearArr(buf);

    kv = KeyValueList_get(&self->list, 2);
    strcat(buf, KeyValue_getVal(kv));
    prArray(buf);
    strcat(str, buf);
    buf[0] = '\0';
    strcat(str, ",");
    clearArr(buf);

    kv = KeyValueList_get(&self->list, 3);
    strcat(str, KeyValue_getVal(kv));
    strcat(str, ",");

    kv = KeyValueList_get(&self->list, 4);
    strcat(buf, KeyValue_getVal(kv));
    prArray(buf);
    strcat(str, buf);
}

KeyValue   * StrStrMap_getWithInd   (StrStrMap * self, int ind)
{
    return KeyValueList_get(&self->list, ind);
}