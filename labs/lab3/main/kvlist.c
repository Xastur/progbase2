#include "kvlist.h"

static void     KeyValueList_realloc  (KeyValueList * self);

int KeyValue_compare(KeyValue * a, KeyValue * b)
{
    int i = strcmp(KeyValue_getKey(a), KeyValue_getKey(b));
    return i;
}

void KeyValueList_init(KeyValueList * self)
{
    self->capacity = 4;
    self->size = 0;
    self->items = malloc(sizeof(KeyValue) * self->capacity);
}

void KeyValueList_deinit(KeyValueList * self)
{
    free(self->items);
}

size_t  KeyValueList_size     (KeyValueList * self)
{
    return self->size;
}

KeyValue    * KeyValueList_get      (KeyValueList * self, int index)
{
    return &self->items[index];
}

void    KeyValueList_set      (KeyValueList * self, int index, KeyValue value)
{
    if (KeyValueList_indexOf(self, value) == -1)
    {
        return;
    }
    KeyValue item = *KeyValueList_get(self, index);
    if (KeyValue_compare(&item, &value) == 0)
    {
        self->items[index] = value;
    }
}

void    KeyValueList_removeAt (KeyValueList * self, int index)
{
    for (int i = index; i < self->size - 1; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size--;
}

void    KeyValueList_add      (KeyValueList * self, KeyValue value)
{
    if (self->size == self->capacity)
    {
        KeyValueList_realloc(self);
    }

    self->items[self->size++] = value;
}  

void    KeyValueList_remove   (KeyValueList * self, KeyValue value)
{
    int index = KeyValueList_indexOf(self, value);
    if (index == -1)
    {
        return;
    }
    KeyValueList_removeAt(self, index);
}  

int     KeyValueList_indexOf  (KeyValueList * self, KeyValue value)
{
    for (int i = 0; i < KeyValueList_size(self); i++)
    {
        if (KeyValue_compare(&value, &self->items[i]) == 0)
        {
            return i;
        }
    }
    return -1;
} 

bool    KeyValueList_contains (KeyValueList * self, KeyValue value)
{
    return KeyValueList_indexOf(self, value) != -1;
}  

void    KeyValueList_clear    (KeyValueList * self)
{
    self->size = 0;
}

void    KeyValueList_print    (KeyValueList * self, FILE * fp)
{
    for (int i = 0; i < self->size; i++)
    {
        fprintf(fp, "Key: %s; Value: %s\n", self->items[i].key, self->items[i].value);
    }
}

static void    KeyValueList_realloc  (KeyValueList * self)
{
    self->capacity *= 2;
    KeyValue *new_item = realloc(self->items, sizeof(KeyValue) * self->capacity);
    if (new_item == NULL)
    {
        fprintf(stderr, "Can not realloc\n");
        exit(EXIT_FAILURE);
    }
    self->items = new_item;
} 

const char * KeyValue_getKey   (KeyValue * self)
{
    return self->key;
}

const char * KeyValue_getVal   (KeyValue * self)
{
    return self->value;
}