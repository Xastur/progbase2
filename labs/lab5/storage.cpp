#include "storage.h"

using namespace std;

Museum Storage::rowToMuseum(const CsvRow & row, string dir_name)
{
    Museum museum;
    museum.id_ = getNewMuseumId(dir_name);
    museum.city_ = row.at(1);
    museum.name_ = row.at(2);
    museum.year_ = atoi(row.at(3).c_str());

    return museum; 
}

CsvRow Storage::MuseumToRow(const Museum & mus)
{
    CsvRow row;

    row.push_back(to_string(mus.id_));
    row.push_back(mus.city_);
    row.push_back(mus.name_);
    row.push_back(to_string(mus.year_));

    return row;
}

Painting Storage::rowToPaint(const CsvRow & row, string dir_name)
{
    Painting paint;

    paint.id_ = getNewPaintingId(dir_name);
    paint.painting_ = row.at(1);
    paint.author_ = row.at(2);
    paint.year_ = atoi(row.at(3).c_str());

    return paint;
}

CsvRow Storage::paintToRow(const Painting & pt)
{
    CsvRow row;

    row.push_back(to_string(pt.id_));
    row.push_back(pt.painting_);
    row.push_back(pt.author_);
    row.push_back(to_string(pt.year_));

    return row;
}

int Storage::getNewMuseumId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string mus_size;
    string paint_size;
    string text;
    int iter = 0;
    while(getline(id_stream, text))
    {
        if (iter == 0)
            mus_size += text;
        else 
            paint_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "id.txt", ios::out);
    id_upd << atoi(mus_size.c_str()) + 1 << endl;
    id_upd << atoi(paint_size.c_str());
    id_upd.close();

    return atoi(mus_size.c_str()) + 1;
}

int Storage::getSizeFromFile(string file_name)
{
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return -1; //error
    }
    string text;
    int size = 0;
    while(getline(file, text))
    {
        size += 1;
    }
    return size;
}

int Storage::getNewPaintingId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string mus_size;
    string paint_size;
    string text;
    int iter = 0;
    while(getline(id_stream, text))
    {
        if (iter == 0)
            mus_size += text;
        else 
            paint_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "id.txt", ios::out);
    id_upd << atoi(mus_size.c_str()) << endl;
    id_upd << atoi(paint_size.c_str()) + 1;
    id_upd.close();

    return atoi(paint_size.c_str()) + 1;}

//Museum

bool Storage::load()
{
    //museum
    string file_name = this->dir_name_ + "museums.csv";
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while(getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow & row : table)
    {
        Museum mus;
        mus = rowToMuseum(row, this->dir_name_);
        this->museums_.push_back(mus);
    }
    //painting 
    file_name = this->dir_name_ + "paint.csv";
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str2;
    string row_str2;
    while(getline(file, row_str2))
    {
        text_str2 += row_str2 + '\n';
    }
    file.close();
    CsvTable table2 = Csv::createTableFromString(text_str2);
    for (CsvRow & row2 : table2)
    {
        Painting pt;
        pt = rowToPaint(row2, this->dir_name_);
        this->paints_.push_back(pt);
    } 

    return true;
}

bool Storage::save()
{
    //museum
    string file_name = this->dir_name_ + "museums.csv";
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable mus_table;
    for (Museum & m : this->museums_)
    {
        CsvRow row = MuseumToRow(m);
        mus_table.push_back(row);
    }
    file << Csv::createStringFromTable(mus_table);
    file.close();
    //painting
    string file_name2 = this->dir_name_ + "paint.csv";
    file.open(file_name2, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable paint_table;
    for (Painting & pt : this->paints_)
    {
        CsvRow row = paintToRow(pt);
        paint_table.push_back(row);
    }
    file << Csv::createStringFromTable(paint_table);
    file.close();    
    return true;
}

void Storage::clearIdFile()
{
    ofstream file;
    file.open(this->dir_name_ + "id.txt", ios::out);
    if (file.fail())
    {
        cerr << "Error. Can't open " << this->dir_name_ + "id.txt" << endl;
        abort();
    }
    file << 0 << "\n" << 0;
    file.close();
}

void Storage::deleteAll()
{
    this->museums_.clear();
    this->paints_.clear();
}

vector<Museum> Storage::getAllMuseums(void)
{
    return this->museums_;
}

optional<Museum> Storage::getMuseumById(int museum_id)
{
    int index;
    for (Museum & mus : this->museums_)
    {
        index = mus.id_;
        if (index == museum_id)
        {
            return mus;
        }
    }
    return nullopt;
}

bool Storage::updateMuseum(const Museum & museum)
{
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == museum.id_)
        {
            this->museums_.at(i).city_ = museum.city_;
            this->museums_.at(i).name_ = museum.name_;
            this->museums_.at(i).year_ = museum.year_;
            return true;
        }
    }
    return false;
}

bool Storage::removeMuseum(int museum_id)
{
    int index;
    size_t mus_id = museum_id;
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == mus_id)
        {
            index = i;
            break;
        }
        if (i == museums.size() - 1)
            return false;
    }
    this->museums_.erase(this->museums_.begin() + index);
    return true;
}

int Storage::insertMuseum(const Museum & museum)
{
    int index = getNewMuseumId(this->dir_name_);
    Museum new_mus = museum;
    new_mus.id_ = index;
    this->museums_.push_back(new_mus);
    return index;
}

//Paintings

vector<Painting> Storage::getAllPaintings(void)
{ 
    return this->paints_;
}

optional<Painting> Storage::getPaintingById(int paint_id)
{
    int index;
    for (Painting & pt : this->paints_)
    {
        index = pt.id_;
        if (index == paint_id)
        {
            return pt;
        }
    }
    return nullopt;
}
bool Storage::updatePainting(const Painting & paint)
{
    auto paintings = getAllPaintings();
    for (size_t i = 0; i < paintings.size(); i++)
    {
        Painting pt = paintings.at(i);
        if (pt.id_ == paint.id_)
        {
            this->paints_.at(i).painting_ = paint.painting_;
            this->paints_.at(i).author_ = paint.author_;
            this->paints_.at(i).year_ = paint.year_;
            return true;
        }
    }
    return false;
}

bool Storage::removePainting(int paint_id)
{
    int index;
    size_t p_id = paint_id;
    auto paintings = getAllPaintings();
    for (size_t i = 0; i < paintings.size(); i++)
    {
        Painting pt = paintings.at(i);
        if (pt.id_ == p_id)
        {
            index = i;
            break;
        }
        if (i == paintings.size() - 1)
            return false;
    }
    this->paints_.erase(this->paints_.begin() + index);
    return true;
}

int Storage::insertPainting(const Painting & paint)
{
    int index = getNewPaintingId(this->dir_name_);
    Painting new_paint = paint;
    new_paint.id_ = index;
    this->paints_.push_back(new_paint);
    return index;
}