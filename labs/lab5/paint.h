#include <string>

using namespace std;
struct Painting 
{
    size_t id_;
    string painting_;
    string author_;
    size_t year_;
};