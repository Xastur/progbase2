#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.h"
#include "museum.h"
#include "paint.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
    const string dir_name_;

    vector<Museum> museums_;
    vector<Painting> paints_;

    static Museum rowToMuseum(const CsvRow & row, string dir_name);
    static CsvRow MuseumToRow(const Museum & mus);
    static Painting rowToPaint(const CsvRow & row, string dir_name);
    static CsvRow paintToRow(const Painting & pt);

    static int getNewMuseumId(string dir_name);
    static int getNewPaintingId(string dir_name);

    int getSizeFromFile(string file_name);

public:
    Storage(const string & dir_name) : dir_name_(dir_name) { }

    bool load();
    bool save();
    void clearIdFile();
    void deleteAll();
    // museums
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    // paintings
    vector<Painting> getAllPaintings(void);
    optional<Painting> getPaintingById(int paint_id);
    bool updatePainting(const Painting &paint);
    bool removePainting(int paint_id);
    int insertPainting(const Painting &paint);
};