#include <fstream>
#include <iostream>
#include <istream>

#include "storage.h"
#include "cui.h"

using namespace std;

int main()
{
    Storage storage("/home/dima/progbase2/labs/lab5/data/");
    if (!storage.load())
    {
        cerr << "Can't load storage" << endl;
        abort();
    }

    Cui cui(&storage);    
    cui.show();
    return 0;
}