#include <string>

using namespace std;

struct Museum 
{
    size_t id_;
    string city_;
    string name_;
    size_t year_;
};