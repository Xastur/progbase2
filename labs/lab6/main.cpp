#include <fstream>
#include <iostream>
#include <istream>

#include "csv_storage.h"
#include "xml_storage.h"
#include "storage.h"
#include "cui.h"
#include "iostream"

using namespace std;

int main()
{
    cout << "Enter data type: ";
    string data_type;
    cin >> data_type;
    if (strcmp(data_type.c_str(), "xml") == 0)
    {
        XmlStorage xml_storage("../lab6/data/xml/");
        Storage * storagePtr = &xml_storage;

        if (!storagePtr->load())
        {
            cerr << "Can't load storage" << endl;
            abort();
        }

        Cui cui(storagePtr);
        cui.show();
    }
    else if (strcmp(data_type.c_str(), "csv") == 0)
    {
        CsvStorage csv_storage("../lab6/data/csv/");
        Storage * storagePtr = &csv_storage;

        if (!storagePtr->load())
        {
            cerr << "Can't load storage" << endl;
            abort();
        }

        Cui cui(storagePtr);
        cui.show();
    } 
    else
    {
        cout << "Such data type doesn't exist" << endl;
    }

    return 0;
}
