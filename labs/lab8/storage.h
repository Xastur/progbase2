#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "museum.h"
#include "paint.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
public:
    virtual ~Storage() {}

    virtual bool open() = 0;
    virtual bool close() = 0;
    virtual void clearIdFile() = 0;
    virtual void deleteAll() = 0;
    // museums
    virtual vector<Museum> getAllMuseums(void) = 0;
    virtual optional<Museum> getMuseumById(int museum_id) = 0;
    virtual bool updateMuseum(const Museum & museum) = 0;
    virtual bool removeMuseum(int museum_id) = 0;
    virtual int insertMuseum(const Museum & museum) = 0;
    // paintings
    virtual vector<Painting> getAllPaintings(void) = 0;
    virtual optional<Painting> getPaintingById(int paint_id) = 0;
    virtual bool updatePainting(const Painting &paint) = 0;
    virtual bool removePainting(int paint_id) = 0;
    virtual int insertPainting(const Painting &paint) = 0;
};
