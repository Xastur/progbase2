#pragma once

#include <string>

using namespace std;

struct Painting 
{
    int id_;
    string painting_;
    string author_;
    int year_;
};
