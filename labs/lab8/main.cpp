#include <fstream>
#include <iostream>
#include <istream>

#include "csv_storage.h"
#include "xml_storage.h"
#include "storage.h"
#include "cui.h"
#include "iostream"
#include "sqlite_storage.h"

using namespace std;

int main()
{
    Storage * storagePtr = nullptr;
    cout << "Enter data type: ";
    string data_type;
    cin >> data_type;
    if (strcmp(data_type.c_str(), "xml") == 0)
    {
        XmlStorage * xml_storage = new XmlStorage("../lab8/data/xml/");
        storagePtr = xml_storage;
    }
    else if (strcmp(data_type.c_str(), "csv") == 0)
    {
        CsvStorage * csv_storage = new CsvStorage("../lab8/data/csv/");
        storagePtr = csv_storage;
    } 
    else if (strcmp(data_type.c_str(), "sql") == 0)
    {
        SqliteStorage * sql_storage = new SqliteStorage("../lab8/data/sqlite/");
        storagePtr = sql_storage;
    }
    else
    {
        cout << "Such data type doesn't exist" << endl;
        return 0;
    }

    if (!storagePtr->open())
    {
        cerr << "Can't open storage" << endl;
        return 1;
    }

    Cui cui(storagePtr);
    cui.show();

    return 0;
}
