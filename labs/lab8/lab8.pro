QT += xml sql
QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    xml_storage.cpp \
    cui.cpp \
    csv_storage.cpp \
    csv.cpp \
    sqlite_storage.cpp

unix|win32: LIBS += -lprogbase

HEADERS += \
    xml_storage.h \
    storage.h \
    paint.h \
    optional.h \
    museum.h \
    cui.h \
    csv_storage.h \
    csv.h \
    sqlite_storage.h

DISTFILES += \
    doc/КП81_ЛР6_Лещенко_Дмитро.docx \
    data/xml/museums.xml \
    data/xml/paint.xml \
    data/csv/museums.csv \
    data/csv/paint.csv \
    data/id.txt \
    data/sqlite/data.sqlite
