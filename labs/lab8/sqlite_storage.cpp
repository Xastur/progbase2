#include "sqlite_storage.h"

void SqliteStorage::clearIdFile() {}
void SqliteStorage::deleteAll() {}

SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_) + "data.sqlite";
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
      cout << "Error. Can't connect" << endl;
      return false;
    }
    else
    {
        cout << "Connected!" << endl;
    }
    return true;
}

bool SqliteStorage::close()
{
    db_.close();
    return true;
}

// museums

Museum getMuseumFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string name = query.value("name").toString().toStdString();
    string city = query.value("city").toString().toStdString();
    int year = query.value("year").toInt();
    Museum m;
    m.id_ = id;
    m.name_ = name;
    m.city_ = city;
    m.year_ = year;
    return m;
}

vector<Museum> SqliteStorage::getAllMuseums(void)
{
    vector <Museum> museums;
    QSqlQuery query("SELECT * FROM museums");
    while (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        museums.push_back(m);
    }
    return museums;
}

optional<Museum> SqliteStorage::getMuseumById(int museum_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec())
    {
       qDebug() << "get museum error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        return m;
    }
    return nullopt;
}

bool SqliteStorage::updateMuseum(const Museum & museum)
{
    QSqlQuery query;
    query.prepare("UPDATE museums SET name = :name, city = :city, year = :year WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    query.bindValue(":id", museum.id_);

    cout << museum.id_ << endl << museum.name_ << endl << museum.city_ << endl << museum.year_ << endl;

    if (!query.exec())
    {
        qDebug() << "update museum error:" << query.lastError();
        return false;
    }

    return true;
}

bool SqliteStorage::removeMuseum(int museum_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec()){
        qDebug() << "delete museum error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertMuseum(const Museum & museum)
{
    QSqlQuery query;
    query.prepare("INSERT INTO museums (name, city, year) VALUES (:name, :city, :year)");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    if (!query.exec()){
        qDebug() << "inserting museum error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

// paintings

Painting getPaintingFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string painting = query.value("painting").toString().toStdString();
    string author = query.value("author").toString().toStdString();
    int year = query.value("year").toInt();
    Painting p;
    p.id_ = id;
    p.author_ = author;
    p.painting_ = painting;
    p.year_ = year;
    return p;
}

vector<Painting> SqliteStorage::getAllPaintings(void)
{
    vector <Painting> paintings;
    QSqlQuery query("SELECT * FROM paintings");
    while (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        paintings.push_back(p);
    }
    return paintings;
}

optional<Painting> SqliteStorage::getPaintingById(int paint_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM paintings WHERE id = :id");
    query.bindValue(":id", paint_id);
    if (!query.exec())
    {
       qDebug() << "get painting error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        return p;
    }
    return nullopt;
}

bool SqliteStorage::updatePainting(const Painting &paint)
{
    QSqlQuery query;
    query.prepare("UPDATE paintings SET year = :year, painting = :painting, author = :author WHERE id = :id");
    query.bindValue(":painting", QString::fromStdString(paint.painting_));
    query.bindValue(":author", QString::fromStdString(paint.author_));
    query.bindValue(":year", paint.year_);
    query.bindValue(":id", paint.id_);

    if (!query.exec())
    {
        qDebug() << "update painting error:" << query.lastError();
        return false;
    }

    return true;
}

bool SqliteStorage::removePainting(int paint_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM paintings WHERE id = :id");
    query.bindValue(":id", paint_id);
    if (!query.exec()){
        qDebug() << "delete painting error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertPainting(const Painting &paint)
{
    QSqlQuery query;
    query.prepare("INSERT INTO paintings (author, painting, year) VALUES (:author, :painting, :year)");
    query.bindValue(":author", QString::fromStdString(paint.author_));
    query.bindValue(":painting", QString::fromStdString(paint.painting_));
    query.bindValue(":year", paint.year_);
    if (!query.exec()){
        qDebug() << "inserting painting error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}
