#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include <QtSql>

#include "storage.h"
#include "museum.h"
#include "paint.h"

class SqliteStorage : public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();
    void clearIdFile();
    void deleteAll();
    // museums
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    // paintings
    vector<Painting> getAllPaintings(void);
    optional<Painting> getPaintingById(int paint_id);
    bool updatePainting(const Painting &paint);
    bool removePainting(int paint_id);
    int insertPainting(const Painting &paint);
};

#endif // SQLITE_STORAGE_H
