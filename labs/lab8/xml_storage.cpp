#include "xml_storage.h"

using namespace std;

int XmlStorage::getNewMuseumId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "../id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string mus_size;
    string paint_size;
    string text;
    int iter = 0;
    while(getline(id_stream, text))
    {
        if (iter == 0)
            mus_size += text;
        else
            paint_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "../id.txt", ios::out);
    id_upd << atoi(mus_size.c_str()) + 1 << endl;
    id_upd << atoi(paint_size.c_str());
    id_upd.close();

    return atoi(mus_size.c_str()) + 1;
}

int XmlStorage::getSizeFromFile(string file_name)
{
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return -1; //error
    }
    string text;
    int size = 0;
    while(getline(file, text))
    {
        size += 1;
    }
    return size;
}

int XmlStorage::getNewPaintingId(string dir_name)
{
    ifstream id_stream;
    id_stream.open(dir_name + "../id.txt");
    if (id_stream.fail())
    {
        cerr << "Error getting id" << endl;
        abort();
    }
    string mus_size;
    string paint_size;
    string text;
    int iter = 0;
    while(getline(id_stream, text))
    {
        if (iter == 0)
            mus_size += text;
        else
            paint_size += text;
        iter++;
    }
    id_stream.close();

    ofstream id_upd;
    id_upd.open(dir_name + "../id.txt", ios::out);
    id_upd << atoi(mus_size.c_str()) << endl;
    id_upd << atoi(paint_size.c_str()) + 1;
    id_upd.close();

    return atoi(paint_size.c_str()) + 1;}

//Museum

Museum QDomElementToMuseum(QDomElement & element, string dir_name)
{
    Museum museum;
    int id = XmlStorage::getNewMuseumId(dir_name);
    museum.id_ = id;
    museum.city_ = element.attribute("city").toStdString();
    museum.name_ = element.attribute("name").toStdString();
    museum.year_ = element.attribute("year").toInt();
    return museum;
}

QDomElement MuseumToDomElement(QDomDocument & doc, Museum & museum)
{
    QDomElement museum_el = doc.createElement("museum");
    int id = museum.id_;
    int year = museum.year_;
    museum_el.setAttribute("id", id);
    museum_el.setAttribute("city", museum.city_.c_str());
    museum_el.setAttribute("name", museum.name_.c_str());
    museum_el.setAttribute("year", year);
    return museum_el;
}


Painting QDomElementToPainting(QDomElement & element, string dir_name)
{
    Painting painting;
    int id = XmlStorage::getNewPaintingId(dir_name);
    painting.id_ = id;
    painting.author_= element.attribute("author").toStdString();
    painting.painting_ = element.attribute("painting").toStdString();
    painting.year_ = element.attribute("year").toInt();
    return painting;
}

QDomElement PaintingToDomElement(QDomDocument & doc, Painting & painting)
{
    QDomElement painting_el = doc.createElement("painting");
    int id = painting.id_;
    int year = painting.year_;
    painting_el.setAttribute("id", id);
    painting_el.setAttribute("author", painting.author_.c_str());
    painting_el.setAttribute("painting", painting.painting_.c_str());
    painting_el.setAttribute("year", year);
    return painting_el;
}

bool XmlStorage::open()
{
    //Museum
    string file_name = this->dir_name_ + "museums.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QFile file(Qfile_name);
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream ts(&file);
    QString text = ts.readAll();
    file.close();

    QDomDocument doc;
    QString errorMsg;
    int errorLine;
    int errorCol;
    if(!doc.setContent(text, &errorMsg, &errorLine, &errorCol))
    {
        qDebug() << "Error parsing Xml text:" << errorMsg;
        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    QDomElement root = doc.documentElement();
    for (int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Museum museum = QDomElementToMuseum(element, this->dir_name_);
        this->museums_.push_back(museum);
    }

    //Painting
    file_name = this->dir_name_ + "paint.xml";
    Qfile_name = QString::fromStdString(file_name);
    QFile file1(Qfile_name);
    if (!file1.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream ts1(&file1);
    QString text1 = ts1.readAll();
    file1.close();

    if(!doc.setContent(text1, &errorMsg, &errorLine, &errorCol))
    {
        qDebug() << "Error parsing Xml text:" << errorMsg;
        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    root = doc.documentElement();
    for (int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Painting painting = QDomElementToPainting(element, this->dir_name_);
        this->paints_.push_back(painting);
    }

    return true;
}

bool XmlStorage::close()
{
    //Museum
    QDomDocument doc;
    QDomElement root = doc.createElement("museums");
    for (Museum & mus: this->museums_)
    {
        QDomElement museum_el = MuseumToDomElement(doc, mus);
        root.appendChild(museum_el);
    }
    doc.appendChild(root);
    QString xml_text = doc.toString(4);

    string file_name = this->dir_name_ + "museums.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QFile file(Qfile_name);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();

    //Painting
    QDomDocument doc1;
    root = doc1.createElement("painting");
    for (Painting & painting: this->paints_)
    {
        QDomElement paint_el = PaintingToDomElement(doc1, painting);
        root.appendChild(paint_el);
    }
    doc1.appendChild(root);
    QString xml_text1 = doc1.toString(4);

    file_name = this->dir_name_ + "paint.xml";
    Qfile_name = QString::fromStdString(file_name);
    QFile file1(Qfile_name);
    if (!file1.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream ts1(&file1);
    ts1 << xml_text1;
    file1.close();

    return true;
}

void XmlStorage::clearIdFile()
{
    ofstream file;
    file.open(this->dir_name_ + "../id.txt", ios::out);
    if (file.fail())
    {
        cerr << "Error. Can't open " << this->dir_name_ + "id.txt" << endl;
        abort();
    }
    file << 0 << "\n" << 0;
    file.close();
}

void XmlStorage::deleteAll()
{
    this->museums_.clear();
    this->paints_.clear();
}

vector<Museum> XmlStorage::getAllMuseums(void)
{
    return this->museums_;
}

optional<Museum> XmlStorage::getMuseumById(int museum_id)
{
    int index;
    for (Museum & mus : this->museums_)
    {
        index = mus.id_;
        if (index == museum_id)
        {
            return mus;
        }
    }
    return nullopt;
}

bool XmlStorage::updateMuseum(const Museum & museum)
{
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == museum.id_)
        {
            this->museums_.at(i).city_ = museum.city_;
            this->museums_.at(i).name_ = museum.name_;
            this->museums_.at(i).year_ = museum.year_;
            return true;
        }
    }
    return false;
}

bool XmlStorage::removeMuseum(int museum_id)
{
    int index;
    int mus_id = museum_id;
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == mus_id)
        {
            index = i;
            break;
        }
        if (i == museums.size() - 1)
            return false;
    }
    this->museums_.erase(this->museums_.begin() + index);
    return true;
}

int XmlStorage::insertMuseum(const Museum & museum)
{
    int index = getNewMuseumId(this->dir_name_);
    Museum new_mus = museum;
    new_mus.id_ = index;
    this->museums_.push_back(new_mus);
    return index;
}

//Paintings

vector<Painting> XmlStorage::getAllPaintings(void)
{
    return this->paints_;
}

optional<Painting> XmlStorage::getPaintingById(int paint_id)
{
    int index;
    for (Painting & pt : this->paints_)
    {
        index = pt.id_;
        if (index == paint_id)
        {
            return pt;
        }
    }
    return nullopt;
}
bool XmlStorage::updatePainting(const Painting & paint)
{
    auto paintings = getAllPaintings();
    for (size_t i = 0; i < paintings.size(); i++)
    {
        Painting pt = paintings.at(i);
        if (pt.id_ == paint.id_)
        {
            this->paints_.at(i).painting_ = paint.painting_;
            this->paints_.at(i).author_ = paint.author_;
            this->paints_.at(i).year_ = paint.year_;
            return true;
        }
    }
    return false;
}

bool XmlStorage::removePainting(int paint_id)
{
    int index;
    int p_id = paint_id;
    auto paintings = getAllPaintings();
    for (size_t i = 0; i < paintings.size(); i++)
    {
        Painting pt = paintings.at(i);
        if (pt.id_ == p_id)
        {
            index = i;
            break;
        }
        if (i == paintings.size() - 1)
            return false;
    }
    this->paints_.erase(this->paints_.begin() + index);
    return true;
}

int XmlStorage::insertPainting(const Painting & paint)
{
    int index = getNewPaintingId(this->dir_name_);
    Painting new_paint = paint;
    new_paint.id_ = index;
    this->paints_.push_back(new_paint);
    return index;
}
