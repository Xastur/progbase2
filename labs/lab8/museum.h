#pragma once

#include <string>

using namespace std;

struct Museum 
{
    int id_;
    string name_;
    string city_;
    int year_;
};
