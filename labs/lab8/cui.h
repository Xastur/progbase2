#pragma once

#include "csv_storage.h"
#include <progbase.h>
#include <progbase/console.h>

class Cui
{
    Storage * const storage_;

    void mainMenu();
    void Menu(int entity_id);
    void UpdateMenu(int entity_id);
    void DeleteMenu(int entity_id);
    void InsertMenu(int entity_id);
    void CloseMenu();

    void printMuseum   (vector<Museum> & mus);
    void printPainting   (vector<Painting> & pt);

public:
    Cui(Storage * storage): storage_(storage) {}
    //
    void show();
};
