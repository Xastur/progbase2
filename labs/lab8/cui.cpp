#include "cui.h"

void Cui::mainMenu()
{
    int input;
    while (true)
    {
        Console_clear();
        cout << "Enter entity" << endl;
        cout << "1. Museum" << endl;
        cout << "2. Painting" << endl;
        cout << "3. Clear file with IDs" << endl;
        cout << "4. Exit" << endl;

        std::cin >> input;

        if (input == 1 || input == 2)
        {
            Menu(input);    
        }
        else if (input == 3)
        {
            storage_->clearIdFile();
            storage_->deleteAll();
            storage_->open();
        }
        else if (input == 4)
        {
            storage_->clearIdFile();
            storage_->close();
            Console_clear();
            exit(EXIT_SUCCESS);
        }
        else 
        {
            continue;
        }
    }
}

void Cui::Menu(int entity_id)
{
    int input;

    while (true)
    {
        Console_clear();

        if (entity_id == 1)
        {
            cout << "Enter option" << endl;
            cout << "1. Insert museum" << endl;
            cout << "2. Remove museum" << endl;
            cout << "3. Update museum" << endl;
            cout << "4. Back" << endl;

            vector<Museum> museum = storage_->getAllMuseums();
            printMuseum(museum);
        }
        else 
        {
            cout << "Enter option" << endl;
            cout << "1. Insert painting" << endl;
            cout << "2. Remove painting" << endl;
            cout << "3. Update painting" << endl;
            cout << "4. Back" << endl;

            vector<Painting> painting = storage_->getAllPaintings();
            printPainting(painting);
        }
            
        std::cin >> input;

        if (input == 1)
        {
            InsertMenu(entity_id);
        }
        else if (input == 2)
        {
            DeleteMenu(entity_id);
        }
        else if (input == 3)
        {
            UpdateMenu(entity_id);
        }
        else if (input == 4)
        {
            mainMenu();
        }
        else 
        {
            continue;
        }
    }
}

void Cui::UpdateMenu(int entity_id)
{
    Console_clear();
    cout << "Update menu" << endl;

    size_t index_;
    cout << "Enter index: ";
    cin >> index_;

    optional <Museum> opt = storage_->getMuseumById(index_);
    if (!opt)
    {
        cout << "Such id doesn't exist\nPress ENTER to continue" << endl;
        Console_getChar();
        return;
    }

    if (entity_id == 1)
    {
        string city_;
        cout << "Enter city: ";
        cin >> city_;

        string name_;
        cout << "Enter museum: ";
        cin >> name_;

        size_t year_;
        cout << "Enter year: ";
        cin >> year_;

        Museum museum = opt.value();
        museum.id_ = index_;
        museum.city_ = city_;
        museum.name_ = name_;
        museum.year_ = year_;

        if (!storage_->updateMuseum(museum))
        {
            cout << "Oops, can't update" << endl;
            Console_getChar();
        }
    }
    else
    {
        string painting_;
        cout << "Enter painting: ";
        cin >> painting_;

        string author_;
        cout << "Enter author: ";
        cin >> author_;

        size_t year_;
        cout << "Enter year: ";
        cin >> year_;

        Painting painting;
        painting.id_ = index_;
        painting.painting_ = painting_;
        painting.author_ = author_;
        painting.year_ = year_;

        storage_->updatePainting(painting);
    }
}

void Cui::DeleteMenu(int entity_id)
{
    Console_clear();
    cout << "Delete menu" << endl;

    if (entity_id == 1)
    {
        cout << "Enter museum id to delete" << endl;

        string id;
        cin >> id;

        if(!storage_->removeMuseum(atoi(id.c_str())))
        {
            cout << "Such id doesn't exist.\nPress ENTER to continue" << endl;
            Console_getChar();
        }
    }
    else
    {
        cout << "Enter painting id to delete" << endl;

        string id;
        cin >> id;

        if(!storage_->removePainting(atoi(id.c_str())))
        {
            cout << "Such id doesn't exist.\nPress ENTER to continue" << endl;
            Console_getChar();
        }
    }
}

void Cui::InsertMenu(int entity_id)
{
    Console_clear();
    cout << "Insert menu" << endl;

    if (entity_id == 1)
    {
        string city_;
        cout << "Enter city: ";
        cin >> city_;

        string name_;
        cout << "Enter museum: ";
        cin >> name_;

        size_t year_;
        cout << "Enter year: ";
        cin >> year_;

        Museum museum;
        museum.city_ = city_;
        museum.name_ = name_;
        museum.year_ = year_;

        storage_->insertMuseum(museum);
   }
    else
    {
        string painting_;
        cout << "Enter painting: ";
        cin >> painting_;

        string author_;
        cout << "Enter author: ";
        cin >> author_;

        int year_;
        cout << "Enter year: ";
        cin >> year_;

        Painting painting;
        painting.painting_ = painting_;
        painting.author_ = author_;
        painting.year_ = year_;

        if (storage_->insertPainting(painting) == 0)
        {
            abort();
        }
    }
}

void Cui::show()
{
    Console_clear();
    mainMenu();
    Console_clear();
}

void Cui::CloseMenu()
{
    if(!storage_->close())
    {
        cerr << "Error 5. Can not open output file" << endl;
        abort();
    }
}

void Cui::printMuseum   (vector<Museum> & mus)
{
    printf("\n");
    for (Museum & m : mus)
    {
        printf("%i,", m.id_);
        printf("%s,", m.city_.c_str());
        printf("%s,", m.name_.c_str());
        printf("%i\n", m.year_);
    }
}

void Cui::printPainting   (vector<Painting> & pt)
{
    printf("\n");
    for (Painting & p : pt)
    {
        printf("%i,", p.id_);
        printf("%s,", p.painting_.c_str());
        printf("%s,", p.author_.c_str());
        printf("%i\n", p.year_);
    }
}
