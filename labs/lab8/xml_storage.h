#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.h"
#include "museum.h"
#include "paint.h"
#include "storage.h"

#include "QString"
#include "QFile"
#include "QDebug"
#include "QtXml"

using std::string;
using std::vector;

class XmlStorage : public Storage
{
    const string dir_name_;

    vector<Museum> museums_;
    vector<Painting> paints_;

    int getSizeFromFile(string file_name);
//    static Painting QDomElementToPainting(QDomElement & element, string dir_name);
//    static QDomElement PaintingToDomElement(QDomDocument & doc, Painting & painting);

public:
    XmlStorage(const string & dir_name) : dir_name_(dir_name) { }

    static int getNewMuseumId(string dir_name);
    static int getNewPaintingId(string dir_name);

    bool open();
    bool close();
    void clearIdFile();
    void deleteAll();
    // museums
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    // paintings
    vector<Painting> getAllPaintings(void);
    optional<Painting> getPaintingById(int paint_id);
    bool updatePainting(const Painting &paint);
    bool removePainting(int paint_id);
    int insertPainting(const Painting &paint);
};
