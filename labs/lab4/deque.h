#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstdbool>

class Deque
{
    char **items_;
    int first_;
    int last_;
    int capacity_;
    int size_;

    void realloc(int new_capacity);
    void moveArray(int x);
    // char * operator [] (int index);

  public:

    Deque();
    ~Deque();

    void pushFront(char *str);
    void pushBack(char *str);
    char * popFront();
    char * popBack();
    char * front();
    char * back();
    void print();
    int size();
};