#include "deque.h"

Deque::Deque()
{
    this->capacity_ = 4;
    this->first_ = 0;
    this->last_ = 0;
    this->size_ = 0;
    this->items_ = new char * [this->capacity_];
    
    if (this->items_ == NULL)
    {
        printf("Error. Can not allocate\n");
        exit(EXIT_FAILURE);
    }
}

Deque::~Deque()
{
    delete[] this->items_;
}

void Deque::pushFront(char *str)
{
    if (this->size_ == 0)
    {
        this->items_[this->first_] = str;
        this->size_ += 1;
        return;
    }

    if (this->last_ + 1 == this->capacity_)
    {
        this->capacity_ *= 2;
        realloc(this->capacity_);

        printf("this->cap: %i\n", this->capacity_);
    }

    moveArray(1);
    this->items_[this->first_] = str;
    this->size_ += 1;
}

void Deque::pushBack(char *str)
{
    if (this->size_ == 0)
    {
        this->size_ += 1;
        this->items_[this->first_] = str;
        return;
    }

    if (this->last_ + 1 == this->capacity_)
    {
        this->capacity_ *= 2;
        realloc(this->capacity_);
        printf("this->cap: %i\n", this->capacity_);
    }

    this->last_ += 1;
    this->items_[this->last_] = str;
}

char * Deque::popFront()
{
    if (this->size_ == 0)
        return NULL;

    char * del = this->items_[this->first_];
    moveArray(-1);
    return del;
}

char * Deque::popBack()
{
    if (this->size_ == 0)
        return NULL;

    char * del = this->items_[this->last_];
    this->last_ -= 1;
    return del;
}

char * Deque::front()
{
    if (this->size_ > 0)
        return this->items_[this->first_];

    return NULL;
}

char * Deque::back()
{
    if (this->size_ > 0)
        return this->items_[this->last_];

    return NULL;
}

void Deque::print()
{
    if (this->size_ == 0)
        return;

    for (int i = 0; i <= this->last_; i++)
    {
        printf("%s ", this->items_[i]);
    }
    printf("\n");
}

void Deque::moveArray(int x)
{
    if (x < 0 && this->size_ > 0)
    {
        for (int i = 1; i <= this->last_; i++)
        {
            this->items_[i + x] = this->items_[i];
        }
        this->last_ -= 1;
        return;
    }

    if (this->size_ == 0)
        return;

    if (this->last_ + x >= this->capacity_)
        return;

    for (int i = this->last_; i >= 0; i--)
    {
        this->items_[i + x] = this->items_[i];
    }
    this->last_ += 1;
    return;
}

int Deque::size()
{
    return this->last_;
}

// char * Deque::operator[](int index)
// {
//     if (index < 0 || index > this->size())
//     {
//         abort();
//     }
//     return this->items_[index];   
// }

void Deque::realloc(int new_capacity)
{
    char **new_arr = new char * [new_capacity];

    for (int i = 0; i < this->capacity_; i++)
    {
        new_arr[i] = this->items_[i];
    }

    delete[] this->items_;

    this->items_ = new_arr;
    this->capacity_ = new_capacity;   
}