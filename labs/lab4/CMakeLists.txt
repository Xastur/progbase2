cmake_minimum_required(VERSION 3.0.0)

project(a.out)

file(GLOB SOURCES "*.cpp")

add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME} progbase m)

set(CMAKE_C_FLAGS "-g -std=c++17 -Werror -Wall -pedantic-errors -Wno-unused -fsanitize=address -lprogbase")