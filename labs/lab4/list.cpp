#include "list.h"

List::List()
{
    this->capacity_ = 4;
    this->len_ = 0;
    this->str_ = new char * [capacity_];
}

List::~List()
{
    delete[] this->str_;
}

void List::addElement(char * str_)
{
    if (this->len_ == this->capacity_)
    {
        realloc(this->capacity_ * 2);
    }
    this->str_[this->len_] = str_;
    this->len_ += 1;
}

void List::print()
{
    if (this->len_ == 0)
    {
        printf("List is empty\n");
        return;
    }

    for (int i = 0; i < this->len_; i++)
    {
        printf("%s ", this->str_[i]);
    }
    printf("\n");
}

void List::sortList()
{
    for (int i = 0; i < this->len_ - 1; i++)
    {
        if (strlen(this->str_[i]) >= 10)
        {
            for (int j = i + 1; j < this->len_; j++)
            {
                if (strlen(this->str_[j]) < 10)
                {
                    swap(i, j);
                    break;
                }
            }
        }
    }

    List::test();
}

void List::swap(int i, int j)
{
    char *str_ = this->str_[i]; 
    this->str_[i] = this->str_[j];
    this->str_[j] = str_;
}

void List::test()
{
    for (int i = 0; i < this->len_ - 1; i++)
    {
        if (strlen(this->str_[i]) >= 10 && strlen(this->str_[i + 1]) < 10)
        {
            printf("Error testing string\n");
            break;
        }
    }
}

int List::size()
{
    return this->len_;
}

char * & List::operator[](int index)
{
    if (index < 0 || index > this->size())
    {
        abort();
    }
    return this->str_[index];
}

void List::realloc(int new_capacity)
{
    char **new_arr = new char * [new_capacity];

    for (int i = 0; i < this->capacity_; i++)
    {
        new_arr[i] = this->str_[i];
    }

    delete[] this->str_;

    this->str_ = new_arr;
    this->capacity_ = new_capacity;
}