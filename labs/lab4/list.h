#pragma once

#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cstring>
#include <iostream>

class List
{
    char **str_;
    int capacity_;
    int len_;

    void realloc(int new_capacity);
    void swap(int i, int j);
    void test();

  public:

    List();
    ~List();

    void addElement(char *str);
    void print();
    int  size();
    void sortList();
    char * & operator [] (int index);
};
