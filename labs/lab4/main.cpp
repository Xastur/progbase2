#include "list.h"
#include "deque.h"

int main()
{
    List list = List();

    FILE *fp = fopen("data.txt", "r");
    if (fp == nullptr)
        exit(EXIT_FAILURE);

    fseek(fp, 0, SEEK_END);
    const int arr_len = ftell(fp); 
    fseek(fp, 0, SEEK_SET);

    char * ch = new char[arr_len + 1]; 
    int i = 0;
    int j = 0;
    ch[i++] = fgetc(fp);

    while (ch[i - 1] != EOF)
    {
        if (ch[i - 1] == '\n')
        {
            ch[i - 1] = '\0';

            list.addElement(&ch[j]);

            j = i;
        }
        if (arr_len == i)
        {
            list.addElement(&ch[j]);
        }
        ch[i++] = fgetc(fp);
    }

    ch[i - 1] = '\0';
    fclose(fp);

    printf("List: ");
    list.print();
    list.sortList();

    printf("Sorted list: ");

    list.print();

    int list_size = list.size();

    Deque deque1;
    Deque deque2;

    for (int i = 0; i < list_size; i++)
    {
        if (i % 2 == 1)
            deque1.pushFront(list[i]);

        else
            deque2.pushBack(list[i]);
    }

    printf("First deque: ");
    deque1.print();

    printf("Second deque: ");

    deque2.print();

    List new_list = List();

    for (int i = 0; i <= deque1.size(); i++)
    {
        new_list.addElement(deque1.popBack());
        i--;
    }
    for (int i = 0; i <= deque2.size(); i++)
    {
        new_list.addElement(deque2.popFront());
        i--;
    }

    printf("New list: ");

    new_list.print();

    delete[] ch;

    return 0;
}