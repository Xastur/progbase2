#include "paintingswindow.h"
#include "ui_paintingswindow.h"

PaintingsWindow::PaintingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PaintingsWindow)
{
    ui->setupUi(this);
}

PaintingsWindow::~PaintingsWindow()
{
    delete storage_;
    delete ui;
}

void PaintingsWindow::load(QString file_name)
{
    qDebug() << "mus_id: " << museum_id_;
    qDebug() << "user_id: " << user_id_;

    file_name_ = file_name;
    SqliteStorage * sql_storage = new SqliteStorage(file_name_.toStdString());
    storage_ = sql_storage;

    if (!storage_->open())
    {
        qDebug() << "Can't open storage in paintingswindow.cpp";
    }
    else
        qDebug() << "Opened storage in paintingswindow.cpp";

    vector<Painting> paintings = storage_->getAllMuseumPaintings(museum_id_, user_id_);

    for (size_t i = 0; i < paintings.size(); i++)
    {
        cout << paintings.at(i).painting_ << endl;

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(paintings.at(i));

        QString name = QString::fromStdString(paintings.at(i).painting_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
    }

    ui->listWidget->setEnabled(true);
    ui->pushButton_add->setEnabled(true);
}

void PaintingsWindow::on_pushButton_add_clicked()
{
    add_window = new AddPaintingWindow(this);
    add_window->show();
    connect(add_window, SIGNAL(sendPainting(Painting*)), this, SLOT(receivePainting(Painting*)));
}

void PaintingsWindow::on_pushButton_close_clicked()
{
    PaintingsWindow::close();
}

void PaintingsWindow::receiveUserId(int user_id)
{
    user_id_ = user_id;
}

void PaintingsWindow::receiveMuseumId(int museum_id)
{
    museum_id_ = museum_id;
}

void PaintingsWindow::receivePainting(Painting * painting)
{
    int id = storage_->insertPainting(*painting, user_id_);
    if (id == 0)
    {
        qDebug() << "Can't insert in paintingswindow.cpp";
        return;
    }
    qDebug() << "museum_id: " << museum_id_ << endl << "painting_id: " << id << endl << "user_id: " << user_id_;
    if (!storage_->insertMuseumPainting(museum_id_, id, user_id_))
    {
        qDebug() << "Can't insert MuseumPainting in paintingswindow.cpp";
        return;
    }
    vector<Painting> paintings = storage_->getAllMuseumPaintings(museum_id_, user_id_);

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qPaintingListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(paintings.at(paintings.size() - 1));

    QString name = QString::fromStdString(paintings.at(paintings.size() - 1).painting_);
    qPaintingListItem->setText(name);
    qPaintingListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qPaintingListItem);
}

void PaintingsWindow::receiveUpdPainting(Painting painting)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();
    painting.id_ = p.id_;

    if(!storage_->updatePainting(painting))
    {
        qDebug() << "Can't update";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }

    QListWidgetItem * qMuseumListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(painting);

    QString name = QString::fromStdString(painting.painting_);
    qMuseumListItem->setText(name);
    qMuseumListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qMuseumListItem);
}

void PaintingsWindow::on_pushButton_remove_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
        qDebug() << "Yes was clicked";
    else
    {
        qDebug() << "Yes was *not* clicked";
        return;
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Painting p = var.value<Painting>();
        int ind = p.id_;
        qDebug() << "ind: " << ind;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removePainting(ind))
            qDebug() << "Can't remove painting in paintingswindow.cpp";
        else
        {
            if(!storage_->removeByPaintingId(ind, user_id_))
                qDebug() << "Can't remove connection in paintingswindow.cpp";
            else
                qDebug() << "Removed painting successfully in paintingswindow.cpp";
        }
    }
    qDebug() << file_name_ << endl;

    vector<Painting> painting = storage_->getAllMuseumPaintings(museum_id_, user_id_);
    if (painting.empty())
    {
        ui->pushButton_edit->setEnabled(false);
        ui->pushButton_remove->setEnabled(false);

        ui->authorlabel->setText("-");
        ui->paintinglabel->setText("-");
        ui->yearlabel->setText("-");
        return;
    }
    items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    if (item == nullptr)
    {
        return;
    }
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();

    ui->authorlabel->setText(QString::fromStdString(p.author_));
    ui->paintinglabel->setText(QString::fromStdString(p.painting_));
    ui->yearlabel->setText(QString::fromStdString(to_string(p.year_)));
}

void PaintingsWindow::on_listWidget_itemClicked(QListWidgetItem * painting)
{
    ui->pushButton_remove->setEnabled(true);

    QVariant var = painting->data(Qt::UserRole);
    Painting p = var.value<Painting>();
    qDebug() << "Painting_id in listWidget:" << p.id_;

    string year = to_string(p.year_);
    ui->authorlabel->setText(QString::fromStdString(p.author_));
    ui->paintinglabel->setText(QString::fromStdString(p.painting_));
    ui->yearlabel->setText(QString::fromStdString(year));
    ui->pushButton_edit->setEnabled(true);
}

void PaintingsWindow::on_pushButton_edit_clicked()
{
    edit_paintings_window = new EditPaintingWindow(this);
    edit_paintings_window->show();

    connect(this, SIGNAL(sendPaintingToEditForm(QListWidgetItem*)), edit_paintings_window, SLOT(receivePaintingToEdit(QListWidgetItem*)));
    connect(edit_paintings_window, SIGNAL(sendUpdPainting(Painting)), this, SLOT(receiveUpdPainting(Painting)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendPaintingToEditForm(item));

    ui->pushButton_edit->setEnabled(false);
    ui->pushButton_remove->setEnabled(false);
}

void PaintingsWindow::receievePaintingsToDelete(vector<int> paintings_id)
{
    for (size_t i = 0; i < paintings_id.size(); i++)
    {
        storage_->removePainting(paintings_id.at(i));
    }
}
