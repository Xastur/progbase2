#include "addwindow.h"
#include "ui_addwindow.h"
#include "museum.h"
#include "mainwindow.h"

AddWindow::AddWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddWindow)
{
    ui->setupUi(this);
}

AddWindow::~AddWindow()
{
    delete ui;
}

//void AddWindow::on_pushButton_clicked()
//{

//}

void AddWindow::on_Cancel_bt_clicked()
{
    AddWindow::close();
}

void AddWindow::on_OK_bt_clicked()
{
    Museum * museum = new Museum();
    museum->city_ = ui->City_lineEdit->text().toStdString();
    museum->name_ = ui->Name_lineEdit->text().toStdString();
    museum->year_ = ui->Year_lineEdit->text().toInt();
    emit sendMuseum(museum);

    AddWindow::close();
}
