#pragma once

#include <QMetaType>
#include <string>

using namespace std;

class Museum
{
public:
    Museum();

    int id_ = 0;
    string name_;
    string city_;
    int year_;
};

Q_DECLARE_METATYPE(Museum)
