#include "addpaintingwindow.h"
#include "ui_addpaintingwindow.h"

AddPaintingWindow::AddPaintingWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddPaintingWindow)
{
    ui->setupUi(this);
}

AddPaintingWindow::~AddPaintingWindow()
{
    delete ui;
}

void AddPaintingWindow::on_Cancel_bt_clicked()
{
    AddPaintingWindow::close();
}

void AddPaintingWindow::on_OK_bt_clicked()
{
    Painting * painting = new Painting();
    painting->author_ = ui->Author_lineEdit->text().toStdString();
    painting->painting_ = ui->Painting_lineEdit->text().toStdString();
    painting->year_ = ui->Year_lineEdit->text().toInt();

    emit sendPainting(painting);

    AddPaintingWindow::close();
}
