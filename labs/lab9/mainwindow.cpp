#include "mainwindow.h"
#include "authwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::loadMuseum(QString file_path)
{
    file_name = file_path;
    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    storage_ = sql_storage;
    if(!storage_->open())
    {
        cerr << "Can't open storage: " << file_name.toStdString();
        cerr << "This file doesn't exist" << endl;
        return;
    }

    vector<Museum> museums = storage_->getAllUserMuseums(user_.id);

    for (size_t i = 0; i < museums.size(); i++)
    {
        cout << museums.at(i).name_ << endl;

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(museums.at(i));

        QString name = QString::fromStdString(museums.at(i).name_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
    }

    vector<Painting> vec_painting = storage_->getAllMuseumPaintings(1, user_.id);
    for (size_t i = 0; i < vec_painting.size(); i++)
    {
        cout << "paint: " << vec_painting.at(i).painting_ << endl;
    }

    ui->listWidget->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->Year_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);
}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}

void MainWindow::on_pushButton_1_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new_storage";
    QString folder_path = dialog.getSaveFileName(
        this,
        "Select New Storage Folder",
        current_dir + "/" + default_name + ".sqlite",
        "Folders");

    qDebug() << "Selected directory: " << folder_path;

    if (folder_path.size() == 0)
        return;

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();
        idGenerator::reset();
    }

    ui->pushButton_4->setEnabled(true);
    ui->listWidget->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->Year_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);

    file_name = folder_path;

    SqliteStorage * sql_storage = new SqliteStorage(folder_path.toStdString());
    storage_ = sql_storage;
}

void MainWindow::on_pushButton_2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "",                // directory to start with
                "SQLITE (*.sqlite);;All Files (*)");  // file name filter

    qDebug() << fileName;

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();
        idGenerator::reset();
    }

    file_name = fileName;

    SqliteStorage * sql_storage = new SqliteStorage(fileName.toStdString());
    storage_ = sql_storage;

    if(!storage_->open())
    {
        cerr << "Can't open storage: " << fileName.toStdString();
        cerr << "This file doesn't exist" << endl;
        return;
    }

    vector<Museum> museums = storage_->getAllMuseums();

    for (size_t i = 0; i < museums.size(); i++)
    {
        cout << museums.at(i).name_ << endl;

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(museums.at(i));

        QString name = QString::fromStdString(museums.at(i).name_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
    }

    ui->listWidget->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->Year_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);
}

void MainWindow::on_pushButton_4_clicked() //add
{
    add_window = new AddWindow(this);
    add_window->show();

    connect(add_window, SIGNAL(sendMuseum(Museum*)), this, SLOT(receiveMuseum(Museum*)));
}

vector<int> allPaintingsId(int museum_id)
{
    vector<int> paintings_id;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE museum_id = :museum_id");
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
        qDebug() << "inserting MuseumPainting error int sqlite_storage.cpp:" << query.lastError();
        return paintings_id;
    }
    while (query.next())
    {
        qDebug() << "api: " << query.value("painting_id").toInt();
        paintings_id.push_back(query.value("painting_id").toInt());
    }
    return paintings_id;
}

void MainWindow::on_pushButton_5_clicked() //delete
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
        qDebug() << "Yes was clicked";
    else
    {
        qDebug() << "Yes was *not* clicked";
        return;
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Museum mus = var.value<Museum>();
        int ind = mus.id_;
        qDebug() << "ind: " << ind;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeMuseum(ind))
            qDebug() << "Can't remove museum";
        else
        {
            vector<int> vec = allPaintingsId(ind);
            if (vec.size() > 0)
            {
                connect(this, SIGNAL(removePaintingsOfMuseum(vector<int>)), paintings_window, SLOT(receievePaintingsToDelete(vector<int>)));
                emit(removePaintingsOfMuseum(vec));

                if(!storage_->removeByMuseumId(ind, user_.id))
                    qDebug() << "Can't remove connection in mainwindow.cpp";
            }
        }
    }
    qDebug() << file_name << endl;

    vector<Museum> museum = storage_->getAllUserMuseums(user_.id);

    if (museum.empty())
    {
        ui->pushButton_5->setEnabled(false);
        ui->edit_bt->setEnabled(false);
        ui->pushButton_paintings->setEnabled(false);

        ui->namelabel->setText("-");
        ui->locationlabel->setText("-");
        ui->yearlabel->setText("-");
        return;
    }

    items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    ui->namelabel->setText(QString::fromStdString(mus.name_));
    ui->locationlabel->setText(QString::fromStdString(mus.city_));
    ui->yearlabel->setText(QString::fromStdString(to_string(mus.year_)));
}

void MainWindow::on_edit_bt_clicked() //edit
{
    edit_window = new EditWindow(this);
    edit_window->show();

    connect(this, SIGNAL(sendMuseumToEditForm(QListWidgetItem*)), edit_window, SLOT(receiveMuseumToEdit(QListWidgetItem*)));
    connect(edit_window, SIGNAL(sendUpdMuseum(Museum*)), this, SLOT(receiveUpdMuseum(Museum*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendMuseumToEditForm(item));

    ui->edit_bt->setEnabled(false);
    ui->pushButton_5->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem * museum)
{
    ui->pushButton_5->setEnabled(true);
    ui->edit_bt->setEnabled(true);
    ui->pushButton_paintings->setEnabled(true);

    QVariant var = museum->data(Qt::UserRole);
    Museum mus = var.value<Museum>();
    qDebug() << mus.id_;

    string year = to_string(mus.year_);
    ui->namelabel->setText(QString::fromStdString(mus.name_));
    ui->locationlabel->setText(QString::fromStdString(mus.city_));
    ui->yearlabel->setText(QString::fromStdString(year));
}

void MainWindow::receiveMuseum(Museum * museum)
{
    storage_->insertMuseum(*museum, user_.id);
    vector<Museum> museums = storage_->getAllMuseums();

    QListWidget * ListWidget = ui->listWidget;
    QListWidgetItem * qMuseumListItem = new QListWidgetItem();

    QVariant qVariant;
    qVariant.setValue(museums.at(museums.size() - 1));

    QString name = QString::fromStdString(museums.at(museums.size() - 1).name_);
    qMuseumListItem->setText(name);
    qMuseumListItem->setData(Qt::UserRole, qVariant);

    ListWidget->addItem(qMuseumListItem);
}

void MainWindow::receiveUpdMuseum(Museum * museum)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();
    museum->id_ = mus.id_;

    if(!storage_->updateMuseum(*museum))
    {
        qDebug() << "Can't update";
        return;
    }
    else
    {
        qDebug() << "Updated";
    }

    QListWidgetItem * qMuseumListItem = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant qVariant;
    qVariant.setValue(*museum);

    QString name = QString::fromStdString(museum->name_);
    qMuseumListItem->setText(name);
    qMuseumListItem->setData(Qt::UserRole, qVariant);

    QListWidget * ListWidget = ui->listWidget;
    ListWidget->addItem(qMuseumListItem);
}

void MainWindow::receiveUser(User user)
{
    user_.id = user.id;
    user_.password_hash = user.password_hash;
    user_.username = user.username;
}

void MainWindow::on_pushButton_paintings_clicked()
{
    paintings_window = new PaintingsWindow(this);
    paintings_window->show();

    connect(this, SIGNAL(sendUserIdToPF(int)), paintings_window, SLOT(receiveUserId(int)));
    connect(this, SIGNAL(sendMuseumIdToPF(int)), paintings_window, SLOT(receiveMuseumId(int)));

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    emit(sendUserIdToPF(user_.id));
    emit(sendMuseumIdToPF(mus.id_));

    paintings_window->load(file_name);
}
