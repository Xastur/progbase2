#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "optional.h"
#include "museum.h"
#include "paint.h"
#include "storage.h"

#include "QString"
#include "QFile"
#include "QDebug"
#include "QtXml"

using std::string;
using std::vector;

class XmlStorage : public Storage
{
    const string dir_name_;

    vector<Museum> museums_;
    vector<Painting> paints_;

    int getSizeFromFile(string file_name);
//    static Painting QDomElementToPainting(QDomElement & element, string dir_name);
//    static QDomElement PaintingToDomElement(QDomDocument & doc, Painting & painting);

public:
    XmlStorage(const string & dir_name) : dir_name_(dir_name) { }

    static int getNewMuseumId(string dir_name);
    static int getNewPaintingId(string dir_name);

    bool open();
    bool close();
    void clearIdFile();
    void deleteAll();
    // museums
    vector<Museum> getAllUserMuseums(int user_id);//new
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    int insertMuseum(const Museum & museum, int user_id);

    // paintings
    vector<Painting> getAllUserMuseumPaintings(int user_id, int museum_id);
    vector<Painting> getAllPaintings(void);
    optional<Painting> getPaintingById(int paint_id);
    int insertPainting(const Painting &paint);

    int insertPainting(const Painting & painting, int user_id);
    int removePainting(int painting_id);
    bool updatePainting(const Painting & painting);
    vector<Painting> getAllUserPaintings(int user_id);

    // users
    optional<User> getUserAuth(string & username, string & password);//new

    // links
    vector<Painting> getAllMuseumPaintings(int museum_id, int user_id);//new
    bool insertMuseumPainting(int musuem_id, int painting_id, int user_id);//new
    bool removeMuseumsPainting(int museum_id, int painting_id, int user_id);//new
    bool removeByMuseumId(int museum_id, int user_id);
    bool removeByPaintingId(int paintig_id, int user_id);
};
