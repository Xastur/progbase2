#include "editpaintingwindow.h"
#include "ui_editpaintingwindow.h"

EditPaintingWindow::EditPaintingWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPaintingWindow)
{
    ui->setupUi(this);
}

EditPaintingWindow::~EditPaintingWindow()
{
    delete ui;
}

void EditPaintingWindow::on_push_button_cancel_clicked()
{
    EditPaintingWindow::close();
}

void EditPaintingWindow::on_push_button_ok_clicked()
{
    Painting painting;
    painting.author_ = ui->lineEdit_new_author->text().toStdString();
    painting.painting_ = ui->lineEdit_new_painting->text().toStdString();
    painting.year_ = ui->lineEdit_new_year->text().toInt();
    emit sendUpdPainting(painting);

    EditPaintingWindow::close();
}

void EditPaintingWindow::receivePaintingToEdit(QListWidgetItem * item)
{
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();

    ui->lineEdit_new_author->setText(QString::fromStdString(p.author_));
    ui->lineEdit_new_painting->setText(QString::fromStdString(p.painting_));
    ui->lineEdit_new_year->setText(QString::fromStdString(to_string(p.year_)));
}
