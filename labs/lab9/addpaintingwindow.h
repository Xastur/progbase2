#ifndef ADDPAINTINGWINDOW_H
#define ADDPAINTINGWINDOW_H

#include <QDialog>

#include "paint.h"

namespace Ui {
class AddPaintingWindow;
}

class AddPaintingWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AddPaintingWindow(QWidget *parent = 0);
    ~AddPaintingWindow();

signals:
    void sendPainting(Painting*);

private slots:
    void on_Cancel_bt_clicked();

    void on_OK_bt_clicked();

private:
    Ui::AddPaintingWindow *ui;
};

#endif // ADDPAINTINGWINDOW_H
