#ifndef PAINTINGSWINDOW_H
#define PAINTINGSWINDOW_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>

#include "paint.h"
#include "storage.h"
#include "sqlite_storage.h"
#include "addpaintingwindow.h"
#include "editpaintingwindow.h"

namespace Ui {
class PaintingsWindow;
}

class PaintingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PaintingsWindow(QWidget *parent = 0);
    ~PaintingsWindow();
    void load(QString file_name);

signals:
    void sendPaintingToEditForm(QListWidgetItem*);

private slots:
    void on_pushButton_add_clicked();

    void on_pushButton_close_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_edit_clicked();

    void receiveUserId(int user_id);

    void receiveMuseumId(int museum_id);

    void receivePainting(Painting * painting);

    void receiveUpdPainting(Painting painting);

    void on_listWidget_itemClicked(QListWidgetItem * painting);

    void receievePaintingsToDelete(vector<int>);

private:
    Ui::PaintingsWindow *ui;

    Storage * storage_ = nullptr;
    AddPaintingWindow * add_window;
    EditPaintingWindow * edit_paintings_window;
    QString file_name_;
    int user_id_;
    int museum_id_;
};

#endif // PAINTINGSWINDOW_H
