#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <QString>
#include <QDebug>

#include "optional.h"
#include "museum.h"
#include "csv.h"
#include "storage.h"
#include "idgenerator.h"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
    vector<Museum> museums_;

    static Museum rowToMuseum(const CsvRow & row);
    static CsvRow MuseumToRow(const Museum & mus);

    int getSizeFromFile(string file_name);

public:
    CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

    const string dir_name_;

    bool open();
    bool close();
    void clearIdFile();
    void deleteAll();
    int  size();

    //museum
    vector<Museum> getAllUserMuseums(int user_id);
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    int insertMuseum(const Museum & museum, int user_id);

    int insertPainting(const Painting & painting, int user_id);
    int removePainting(int painting_id);
    bool updatePainting(const Painting & painting);
    vector<Painting> getAllUserPaintings(int user_id);

    // users
    optional<User> getUserAuth(string & username, string & password);

    // links
    vector<Painting> getAllMuseumPaintings(int museum_id, int user_id);
    bool insertMuseumPainting(int musuem_id, int painting_id, int user_id);
    bool removeMuseumsPainting(int museum_id, int painting_id, int user_id);
    bool removeByMuseumId(int museum_id, int user_id);//new
    bool removeByPaintingId(int paintig_id, int user_id);
};
