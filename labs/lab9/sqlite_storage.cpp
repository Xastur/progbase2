#include "sqlite_storage.h"
#include "mainwindow.h"

SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name)
{
    if (!db_.isOpen())
        db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
      cout << "Error. Can't connect" << endl;
      return false;
    }
    else
    {
        cout << "Connected SQL!" << endl;
    }
    return true;
}

int SqliteStorage::size()
{
    return 0;
}

bool SqliteStorage::close()
{
    db_.close();
    return true;
}

// museums

Museum getMuseumFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string name = query.value("name").toString().toStdString();
    string city = query.value("city").toString().toStdString();
    int year = query.value("year").toInt();
    Museum m;
    m.id_ = id;
    m.name_ = name;
    m.city_ = city;
    m.year_ = year;
    return m;
}

vector<Museum> SqliteStorage::getAllMuseums(void)
{
    vector <Museum> museums;
    QSqlQuery query("SELECT * FROM museums");
    while (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        museums.push_back(m);
    }
    return museums;
}

optional<Museum> SqliteStorage::getMuseumById(int museum_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec())
    {
       qDebug() << "get museum error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        return m;
    }
    return nullopt;
}

bool SqliteStorage::updateMuseum(const Museum & museum)
{
    QSqlQuery query;
    query.prepare("UPDATE museums SET name = :name, city = :city, year = :year WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    query.bindValue(":id", museum.id_);

    cout << museum.id_ << endl << museum.name_ << endl << museum.city_ << endl << museum.year_ << endl;

    if (!query.exec())
    {
        qDebug() << "update museum error:" << query.lastError();
        return false;
    }

    return true;
}

bool SqliteStorage::removeMuseum(int museum_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec()){
        qDebug() << "delete museum error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertMuseum(const Museum & museum)
{
    return 0;
}

int SqliteStorage::insertMuseum(const Museum &museum, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO museums (name, city, year, user_id) VALUES (:name, :city, :year, :user_id)");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    query.bindValue(":user_id", user_id);

    if (!query.exec())
    {
        qDebug() << "inserting museum error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

// paintings

Painting getPaintingFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string painting = query.value("painting").toString().toStdString();
    string author = query.value("author").toString().toStdString();
    int year = query.value("year").toInt();
    Painting p;
    p.id_ = id;
    p.author_ = author;
    p.painting_ = painting;
    p.year_ = year;
    return p;
}

optional<Painting> SqliteStorage::getPaintingById(int paint_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM paintings WHERE id = :id");
    query.bindValue(":id", paint_id);
    if (!query.exec())
    {
       qDebug() << "get painting error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        return p;
    }
    return nullopt;
}

int SqliteStorage::insertPainting(const Painting & painting, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO paintings (author, painting, year, user_id) VALUES (:author, :painting, :year, :user_id)");
    query.bindValue(":author", QString::fromStdString(painting.author_));
    query.bindValue(":painting", QString::fromStdString(painting.painting_));
    query.bindValue(":year", painting.year_);
    query.bindValue(":user_id", user_id);
    if (!query.exec()){
        qDebug() << "inserting painting error:" << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    qDebug() << "Last inserted painting id: " << var.toInt();
    return var.toInt();
}

int SqliteStorage::removePainting(int painting_id)
{
    QSqlQuery query;

    query.prepare("DELETE FROM paintings WHERE id = :id");
    query.bindValue(":id", painting_id);
    if (!query.exec())
    {
        qDebug() << "delete painting error:" << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::updatePainting(const Painting & painting)
{
    QSqlQuery query;
    query.prepare("UPDATE paintings SET year = :year, painting = :painting, author = :author WHERE id = :id");
    query.bindValue(":painting", QString::fromStdString(painting.painting_));
    query.bindValue(":author", QString::fromStdString(painting.author_));
    query.bindValue(":year", painting.year_);
    query.bindValue(":id", painting.id_);

    if (!query.exec())
    {
        qDebug() << "update painting error in sqlite_storage.cpp:" << query.lastError();
        return false;
    }

    return true;
}

vector<Painting> SqliteStorage::getAllUserPaintings(int user_id)
{
    QSqlQuery query;
    vector<Painting> vec_p;
    query.prepare("SELECT * FROM paintings WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << "get allUserPaintings error:" << query.lastError();
       return vec_p;
    }
    while (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        vec_p.push_back(p);
    }
    return vec_p;
}

vector<Museum> SqliteStorage::getAllUserMuseums(int user_id)
{
    QSqlQuery query;
    vector<Museum> vec_mus;
    query.prepare("SELECT * FROM museums WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << "get allUserMuseum error:" << query.lastError();
       return vec_mus;
    }
    while (query.next())
    {
        Museum mus = getMuseumFromQuery(query);
        vec_mus.push_back(mus);
    }
    return vec_mus;
}//new

QString hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

// users
optional<User> SqliteStorage::getUserAuth(string & username, string & password)
{
    QSqlQuery query;
    optional<User> opt;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;");
    query.bindValue(":username", QString::fromStdString(username));
    query.bindValue(":password_hash", hashPassword(QString::fromStdString(password)));
    if (!query.exec())
    {
        return nullopt;
    }
    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = username;
        user.password_hash = hashPassword(QString::fromStdString(password)).toStdString();
        opt = user;
        return opt;
    }
    return nullopt;
}//new

// links
vector<Painting> SqliteStorage::getAllMuseumPaintings(int museum_id, int user_id)
{
    vector<Painting> vec_paintings;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE museum_id = :museum_id AND user_id = :user_id;");
    query.bindValue(":user_id", user_id);
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
       qDebug() << "get allMuseumPaintings error:" << query.lastError();
       return vec_paintings;
    }
    while (query.next())
    {
        qDebug() << "painting id: " << query.value("painting_id").toInt();
        optional<Painting> opt = getPaintingById(query.value("painting_id").toInt());
        if (!opt)
        {
            qDebug() << "Error getting opt";
            continue;
        }
        Painting painting = opt.value();
        vec_paintings.push_back(painting);
    }
    return vec_paintings;
}//new

bool isPresent(int museum_id, int painting_id, int user_id)
{
    QSqlQuery query_test;
    query_test.prepare("SELECT museum_id, painting_id, museum_id FROM links WHERE museum_id = :museum_id AND painting_id = :painting_id AND user_id = :user_id");
    query_test.bindValue(":museum_id", museum_id);
    query_test.bindValue(":painting_id", painting_id);
    query_test.bindValue(":user_id", user_id);
    if (!query_test.exec())
    {
        return true;
    }
    if (query_test.next())
    {
        if (query_test.value("museum_id").toInt() == museum_id)
            return true;
    }
    return false;
}

bool SqliteStorage::insertMuseumPainting(int museum_id, int painting_id, int user_id)
{
    int vec_size = SqliteStorage::getAllMuseumPaintings(museum_id, user_id).size();
    if (vec_size == 0)
    {
        qDebug() << "links is empty";
//        return false;
    }
    else if (isPresent(museum_id, painting_id, user_id))
    {
        qDebug() << "MuseumPaintings is present. Error in sqlite_storage.cpp";
        return false;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO links (museum_id, painting_id, user_id) VALUES (:museum_id, :painting_id, :user_id)");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    qDebug() << painting_id;
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "inserting MuseumPainting error int sqlite_storage.cpp:" << query.lastError();
        return false;
    }
    return true;
}//new

bool SqliteStorage::removeMuseumsPainting(int museum_id, int painting_id, int user_id)
{
    if (!isPresent(museum_id, painting_id, user_id))
    {
        return false; // not such painting
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE museum_id = :museum_id AND painting_id = :painting_id AND user_id = :user_id;");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}//new

bool SqliteStorage::removeByMuseumId(int museum_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE museum_id = :museum_id AND user_id = :user_id;");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeByPaintingId(int painting_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE painting_id = :painting_id AND user_id = :user_id;");
    query.bindValue(":painting_id", painting_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}
