#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma once

#include <QMainWindow>
#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "addwindow.h"
#include "museum.h"
#include "storage.h"
#include "idgenerator.h"
#include "ui_mainwindow.h"
#include "csv_storage.h"
#include "editwindow.h"
#include "sqlite_storage.h"
#include "user.h"
#include "paintingswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void loadMuseum(QString file_path);
    ~MainWindow();

signals:
    void sendMuseumToEditForm(QListWidgetItem*);
    void sendUserIdToPF(int user_id);
    void sendMuseumIdToPF(int museum_id);
    void removePaintingsOfMuseum(vector<int>);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_1_clicked();

    void on_pushButton_4_clicked();

    void on_listWidget_itemClicked(QListWidgetItem * item);

    void on_pushButton_5_clicked();

    void receiveMuseum(Museum * museum);

    void receiveUpdMuseum(Museum * museum);

    void on_edit_bt_clicked();

    void receiveUser(User user);

    void on_pushButton_paintings_clicked();

private:
    Ui::MainWindow *ui;
    AddWindow * add_window;
    EditWindow * edit_window;
    PaintingsWindow * paintings_window;

    Storage * storage_ = nullptr;
    QString file_name;
    User user_;
};

#endif // MAINWINDOW_H
