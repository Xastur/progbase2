#include "csv_storage.h"
#include "idgenerator.h"

using namespace std;

Museum CsvStorage::rowToMuseum(const CsvRow & row)
{
    Museum museum;

    if (row.at(0) == "0")
        museum.id_ = idGenerator::id_get();
    else
        museum.id_ = atoi(row.at(0).c_str());

    museum.name_ = row.at(1);
    museum.city_ = row.at(2);
    museum.year_ = atoi(row.at(3).c_str());

    return museum; 
}

CsvRow CsvStorage::MuseumToRow(const Museum & mus)
{
    CsvRow row;

    row.push_back(to_string(mus.id_));
    row.push_back(mus.name_);
    row.push_back(mus.city_);
    row.push_back(to_string(mus.year_));


    return row;
}

int CsvStorage::getSizeFromFile(string file_name)
{
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return -1; //error
    }
    string text;
    int size = 0;
    while(getline(file, text))
    {
        size += 1;
    }
    return size;
}

bool CsvStorage::open()
{
    string file_name = this->dir_name_;
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while(getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow & row : table)
    {
        Museum mus;
        mus = rowToMuseum(row);
        mus.id_ = idGenerator::id_get();
        this->museums_.push_back(mus);
    }

    return true;
}

bool CsvStorage::close()
{
    qDebug() << "Save func" << endl;
    string file_name = this->dir_name_;
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable mus_table;
    for (Museum & m : this->museums_)
    {
        CsvRow row = MuseumToRow(m);
        mus_table.push_back(row);
    }
    file << Csv::createStringFromTable(mus_table);
    file.close();

    return true;
}

void CsvStorage::clearIdFile()
{
    ofstream file;
    file.open(this->dir_name_ + "../id.txt", ios::out);
    if (file.fail())
    {
        cerr << "Error. Can't open " << this->dir_name_ + "id.txt" << endl;
        abort();
    }
    file << 0 << "\n" << 0;
    file.close();
}

void CsvStorage::deleteAll()
{
    this->museums_.clear();
}

int CsvStorage::size()
{
    return this->museums_.size();
}

vector<Museum> CsvStorage::getAllMuseums(void)
{
    return this->museums_;
}

optional<Museum> CsvStorage::getMuseumById(int museum_id)
{
    int index;
    for (Museum & mus : this->museums_)
    {
        index = mus.id_;
        if (index == museum_id)
        {
            return mus;
        }
    }
    return nullopt;
}

bool CsvStorage::updateMuseum(const Museum & museum)
{
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == museum.id_)
        {
            this->museums_.at(i).city_ = museum.city_;
            this->museums_.at(i).name_ = museum.name_;
            this->museums_.at(i).year_ = museum.year_;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeMuseum(int museum_id)
{
    int index;
    int mus_id = museum_id;
    auto museums = getAllMuseums();
    qDebug() << "museums.size before deletion: " << museums.size();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        qDebug() << mus.id_;
        if (mus.id_ == mus_id)
        {
            index = i;
            break;
        }
        if (i == museums.size() - 1)
            return false;
    }
    this->museums_.erase(this->museums_.begin() + index);
    return true;
}

int CsvStorage::insertMuseum(const Museum & museum)
{
    int index = idGenerator::id_get();
    Museum new_mus = museum;
    new_mus.id_ = index;
    this->museums_.push_back(new_mus);
    return index;
}

//-------------------------------------------------------------------//

int CsvStorage::insertMuseum(const Museum & museum, int user_id)
{
    return 1;
}

int CsvStorage::insertPainting(const Painting & painting, int user_id)
{
    return 1;
}

int CsvStorage::removePainting(int painting_id)
{
    return 1;
}

bool CsvStorage::updatePainting(const Painting & painting)
{
    return true;
}

vector<Painting> CsvStorage::getAllUserPaintings(int user_id)
{
    vector<Painting> p;
    return p;
}

vector<Museum> CsvStorage::getAllUserMuseums(int user_id)
{
    vector<Museum>mus;
    return mus;
}

optional<User> CsvStorage::getUserAuth(string & username, string & password)
{
    return nullopt;
}

vector<Painting> CsvStorage::getAllMuseumPaintings(int museum_id, int user_id)
{
    vector<Painting> paints;
    return paints;
}

bool CsvStorage::insertMuseumPainting(int musuem_id, int painting_id, int user_id)
{
    return true;
}

bool CsvStorage::removeMuseumsPainting(int museum_id, int painting_id, int user_id)
{
    return true;
}

bool CsvStorage::removeByMuseumId(int museum_id, int user_id)
{
    return true;
}

bool CsvStorage::removeByPaintingId(int paintig_id, int user_id)
{
    return true;
}
