#pragma once

#include <QMetaType>
#include <string>

using namespace std;

struct Painting 
{
    int id_;
    string painting_;
    string author_;
    int year_;
};

Q_DECLARE_METATYPE(Painting)
