#include "deque.h"

// struct Deque

struct Deque *Deque_init(void)
{
    struct Deque *self = NULL;
    self = malloc(sizeof(struct Deque));
    if (self == NULL)
    {
        printf("Error. Can not allocate\n");
        exit(EXIT_FAILURE);
    }
    self->capacity = 1;
    self->first = 0;
    self->last = 0;
    self->size = 0;
    self->items = malloc(sizeof(char *) * self->capacity);
    if (self == NULL)
    {
        printf("Error. Can not allocate\n");
        exit(EXIT_FAILURE);
    }
    return self;
}

// void

void Deque_pushFront(struct Deque *self, char *str)
{
    if (self->size == 0)
    {
        self->items[self->first] = str;
        self->size += 1;
        return;
    }

    if (self->last == self->capacity)
    {
        self->capacity *= 2;
        char **new_items = realloc(self->items, sizeof(char *) * self->capacity);
        printf("Reallocating... ");
        if (new_items == NULL)
        {
            printf("\nReallocating error\n");
            free(self->items);
            free(self);
            exit(EXIT_FAILURE);
        }
        self->items = new_items;
        printf("self->cap: %i\n", self->capacity);
    }

    Deque_moveArray(self, 1);
    self->items[self->first] = str;
    self->size += 1;
}

void Deque_pushBack(struct Deque *self, char *str)
{
    if (self->size == 0)
    {
        self->size += 1;
        self->items[self->first] = str;
        return;
    }

    if (self->last == self->capacity)
    {
        self->capacity *= 2;
        char **new_items = realloc(self->items, sizeof(char *) * self->capacity);
        printf("Reallocating... ");
        if (new_items == NULL)
        {
            printf("\nReallocating error\n");
            free(self->items);
            free(self);
            exit(EXIT_FAILURE);
        }
        self->items = new_items;
        printf("self->cap: %i\n", self->capacity);
    }

    self->last += 1;
    self->items[self->last] = str;
}

void Deque_popFront(struct Deque *self)
{
    if (self->size == 0)
        return;

    Deque_moveArray(self, -1);
}

void Deque_popBack(struct Deque *self)
{
    if (self->size == 0)
        return;

    self->last -= 1;
}

void Deque_front(struct Deque *self)
{
    if (self->size > 0)
        printf("Front: %s\n", self->items[self->first]);
}

void Deque_back(struct Deque *self)
{
    if (self->size > 0)
        printf("Back: %s\n", self->items[self->last]);
}

void Deque_print(struct Deque *self)
{
    if (self->size == 0)
        return;

    Console_setCursorAttribute(FG_GREEN);

    for (int i = 0; i <= self->last; i++)
    {
        printf("%s ", self->items[i]);
    }
    printf("\n");
    Console_reset();
}

void Deque_deinit(struct Deque *self)
{
    free(self->items);
    free(self);
}

void Deque_moveArray(struct Deque *self, int x)
{
    if (x < 0 && self->size > 0)
    {
        for (int i = 1; i <= self->last; i++)
        {
            self->items[i + x] = self->items[i];
        }
        self->last -= 1;
        return;
    }

    if (self->size == 0)
        return;

    if (self->last + x >= self->capacity)
        return;

    for (int i = self->last; i >= 0; i--)
    {
        self->items[i + x] = self->items[i];
    }
    self->last += 1;
    return;
}

// int 

int Deque_size(struct Deque *self)
{
    return self->last;
}

// char *

char *Deque_getChar(struct Deque *self, int i)
{
    return self->items[i];
}