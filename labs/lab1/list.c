#include "list.h"

// struct List

struct List *List_init(void)
{
    struct List *list = malloc(sizeof(struct List));
    list->capacity = 4;
    list->len = 0;
    list->str = malloc(sizeof(char *) * list->capacity);

    return list;
}

// void

struct List *List_addElement(struct List *self, char *str)
{
    if (self->len == self->capacity)
    {
        char **new_list = NULL;
        self->capacity *= 2;
        new_list = realloc(self->str, sizeof(char*) * self->capacity);
        if (new_list == NULL)
        {
            printf("Can not realloc\n");
            free(self->str);
            free(self);
            exit(EXIT_FAILURE);
        }
        self->str = new_list;
    }
    self->str[self->len] = str;
    self->len += 1;
    return self;
}

void List_print(struct List *self)
{
    if (self->len == 0)
    {
        printf("List is empty\n");
        return;
    }

    struct List *list = self;
    Console_setCursorAttribute(FG_GREEN);

    for (int i = 0; i < list->len; i++)
    {
        printf("%s ", list->str[i]);
    }
    printf("\n");
    Console_reset();
}

void List_deinit(struct List *self)
{
    if (self == NULL)
        return;

    free(self->str);
    free(self);
}

void List_moveList(struct List *self)
{
    struct List *list = self;
    if (list == NULL)
        return;

    for (int i = 0; i < list->len - 1; i++)
    {
        if (strlen(list->str[i]) >= 10)
        {
            for (int j = i + 1; j < list->len; j++)
            {
                if (strlen(list->str[j]) < 10)
                {
                    List_swap(list, i, j);
                    break;
                }
            }
        }
    }

    List_test(self);
}

void List_swap(struct List *self, int i, int j)
{
    if (self == NULL)
        return;

    char *str = self->str[i]; 
    self->str[i] = self->str[j];
    self->str[j] = str;
}

void List_test(struct List *self)
{
    struct List *list = self;
    if (list == NULL)
        return;

    for (int i = 0; i < self->len - 1; i++)
    {
        if (strlen(list->str[i]) >= 10 && strlen(list->str[i + 1]) < 10)
        {
            Console_setCursorAttribute(FG_RED);
            printf("Error testing string\n");
            Console_reset();
            break;
        }
    }
}

// int

int List_size(struct List *self)
{
    return self->len;
}

// char *

char *List_getStr(struct List *self, int i)
{
    return self->str[i];
}