#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <progbase.h>
#include <progbase/console.h>

struct Deque
{
    char **items;
    int first;
    int last;
    int capacity;
    int size;
};

struct Deque *Deque_init(void);

void Deque_pushFront(struct Deque *self, char *str);
void Deque_pushBack(struct Deque *self, char *str);
void Deque_popFront(struct Deque *self);
void Deque_popBack(struct Deque *self);
void Deque_front(struct Deque *self);
void Deque_back(struct Deque *self);
void Deque_print(struct Deque *self);
void Deque_deinit(struct Deque *self);
void Deque_moveArray(struct Deque *self, int x);

int Deque_size(struct Deque *self);

char *Deque_getChar(struct Deque *self, int i);