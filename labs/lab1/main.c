#include "list.h"
#include "deque.h"
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>

int main()
{
    struct List *list = NULL;
    list = List_init();
    Console_setCursorAttribute(BG_INTENSITY_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    Console_reset();
    printf("\n");

    FILE *fp = fopen("data.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    fseek(fp, 0, SEEK_END);
    const int arr_len = ftell(fp); 
    fseek(fp, 0, SEEK_SET);

    char ch[arr_len];
    int i = 0;
    int j = 0;
    ch[i++] = fgetc(fp);

    while (ch[i - 1] != EOF)
    {
        if (ch[i - 1] == '\n')
        {
            ch[i - 1] = '\0';

            list = List_addElement(list, &ch[j]);

            j = i;
        }
        if (arr_len == i)
        {
            list = List_addElement(list, &ch[j]);
        }
        ch[i++] = fgetc(fp);
    }

    ch[i - 1] = '\0';
    fclose(fp);

    Console_setCursorAttribute(FG_CYAN);
    printf("List: ");
    Console_reset();
    List_print(list);
    List_moveList(list);

    Console_setCursorAttribute(FG_CYAN);
    printf("Sorted list: ");
    Console_reset();

    List_print(list);

    struct Deque *deque1 = Deque_init();
    struct Deque *deque2 = Deque_init();

    int list_size = List_size(list);

    for (int i = 0; i < list_size; i++)
    {
        if (i % 2 == 1)
            Deque_pushFront(deque1, List_getStr(list, i));

        else
            Deque_pushBack(deque2, List_getStr(list, i));
    }

    Console_setCursorAttribute(FG_CYAN);
    printf("First deque: ");
    Console_reset();
    Deque_print(deque1);

    Console_setCursorAttribute(FG_CYAN);
    printf("Second deque: ");
    Console_reset();

    Deque_print(deque2);

    struct List *new_list = List_init();

    for (int i = Deque_size(deque1); i >= 0; i--)
    {
        new_list = List_addElement(new_list, Deque_getChar(deque1, i));
    }
    for (int i = 0; i <= Deque_size(deque2); i++)
    {
        new_list = List_addElement(new_list, Deque_getChar(deque2, i));
    }

    Console_setCursorAttribute(FG_CYAN);
    printf("New list: ");
    Console_reset();

    List_print(new_list);

    List_deinit(list);
    List_deinit(new_list);
    Deque_deinit(deque1);
    Deque_deinit(deque2);

    return 0;
}
