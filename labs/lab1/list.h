#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <ctype.h>
#include <string.h>

struct List
{
    char **str;
    int capacity;
    int len;
};

struct List *List_init(void);
struct List *List_addElement(struct List *self, char *str);

void List_print(struct List *self);
void List_deinit(struct List *self);
void List_moveList(struct List *self);
void List_swap(struct List *self, int i, int j);
void List_test(struct List *self);

int  List_size(struct List *self);

char *List_getStr(struct List *self, int i);