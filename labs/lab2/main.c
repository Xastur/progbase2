#include "list.h"
#include "csv.h"
#include "lab.h"

int main(int argc, char *argv[])
{
    struct Museum testList[6] = {
        {"museum1", "city1", 1100, "1 million"},
        {"museum2", "city1", 1200, "2 million"},
        {"museum3", "city3", 1300, "3 million"},
        {"museum4", "city4", 1400, "4 million"},
        {"museum5", "city5", 1500, "5 million"},
    };

    char data_name[20];
    data_name[0] = '\0';

    char out[20];
    out[0] = '\0';

    char mus[20];
    mus[0] = '\0';

    char str[1000];
    str[0] = '\0';
    int ind = 0;

    //

    int opt;
    while ((opt = getopt(argc, argv, "n:o:")) != -1)
    {
        switch (opt)
        {
        case 'n':
            strcpy(mus, optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            strcpy(out, optarg);
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }

    List *table = List_alloc();
    List * pre_table = NULL;
    List *items = NULL;

    if (optind < argc)
    {
        strcpy(data_name, argv[optind]);

        FILE *p = NULL;
        p = fopen(data_name, "r");
        if (p == NULL)
        {
            printf("CAN NOT OPEN %s\n", argv[optind]);
            exit(EXIT_FAILURE);
        }
        char c = getc(p);
        while (c != EOF)
        {
            str[ind++] = c;
            c = getc(p);
        }
        str[ind] = '\0';
        fclose(p);
        Csv_fillTableFromString(table, str);
    }
    else 
    {
        for (int i = 0; i < sizeof(testList) / sizeof(testList[0]) - 1; i++)
        {
            pre_table = List_alloc();
            
            List_add(pre_table, StrOnHeap(testList[i].name));
            List_add(pre_table, StrOnHeap(testList[i].location));
            Csv_addInt(pre_table, testList[i].establ);
            List_add(pre_table, StrOnHeap(testList[i].visitors));

            List_add(table, pre_table);

            pre_table = NULL;
        }
    }

    // 

    printf("Table:\n");
    Csv_printTable(table, stdout);
    printf("----------------------------\n");

    items = createMuseumListFromTable(table);
    
    if (strlen(mus) > 0)
    {
        processItems2(items, mus);
    }

    printf("\nList:");
    List_structPrint(items);
    printf("----------------------------\n");

    if (strlen(out) > 0)
    {
        FILE *pout = NULL;
        pout = fopen(out, "w");

        if (pout == NULL)
        {
            printf("Can not open file %s\n", out);
            exit(EXIT_FAILURE);
        }
        items = List_toTable(items);
        Csv_printTable(items, pout);
        fclose(pout);
    }

    if (strlen(out) > 0)
    {
        Csv_clearTable(items);
    }
    else
    {
        List_clearStruct(items);
        List_free(items);
    } 

    Csv_clearTable(table);
    printf("Done\n");

    return 0;
}