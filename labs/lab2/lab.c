#include "lab.h"

static void   List_sizeMinus(List * self);

static void   List_sizeMinus(List * self)
{
    self->size -= 1;
}

void   processItems(List * items, char * n)
{
    for (int i = 0; i < List_size(items); i++)
    {
        List * row = List_get(items, i);
        if (strcmp(List_get(row, 1), n) == 0)
        {
            break;
        }
        if (i == List_size(items) - 1)
            return;
    }

    int x = 0;

    for (int i = 0; i < List_size(items) + x; i++)
    {
        List *row = List_get(items, i);
        if (strcmp(n, List_get(row, 1)) == 0)
        {
            x = i;
        }
        else 
        {
            List *l = List_get(items, i);
            for (int j = 0; j < List_size(l); j++)
            {
                char *c = List_get(l, j);
                free(c);
            }

            if (i < List_size(items) - 1)
            {
                List *l = List_get(items, i);
                for (int j = i; j < List_size(items) - 1; j++)
                {   
                    List * it1 = List_get(items, j);
                    it1 = List_get(items, j + 1);
                    // items->items[j] = items->items[j + 1]; 
                    if (j == List_size(items) - 2)
                    {
                        List_free(l);
                    }
                }
                i -= 1;
            }
            else 
            {
                List *l = List_get(items, i);
                List_free(l);
            }
            List_sizeMinus(items);
            // items->size -= 1;
        }
    }

    List *row = List_get(items, x);

    for (int i = List_size(row) - 1; i >= 1; i--)
    {
        char *c = List_get(row, i);
        free(c);  
        List_sizeMinus(row);
    }
} 

List * createMuseumListFromTable(List * csvTable)
{
    List *new_list = List_alloc();
    struct Museum *mus = NULL;
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *list = List_get(csvTable, i);
        mus = malloc(sizeof(struct Museum));
        mus->name = (char*)List_get(list, 0);
        mus->location = List_get(list, 1);
        char *c = (char *)List_get(list, 2);
        mus->establ = atoi(c);
        mus->visitors = (char *)List_get(list, 3);
        List_add(new_list, mus);
    }
    return new_list;
}

void   processItems2(List * items, char * n)
{
    for (int i = 0; i < List_size(items); i += 1)
    {
        struct Museum *mus = List_get(items, i);
        if (strcmp(mus->location, n) == 0)
        {
        }
        else 
        {
            List * l = List_get(items, i);
            for (int j = i; j < List_size(items) - 1; j++)
            {
                List_set(items, j, List_get(items, j + 1));
            }
            free(l);
            List_sizeMinus(items);
            i--;
        }
    }
}

List * List_toTable(List * self)
{
    char str[1000];
    List_valueFromListToStr(self, str, 1000);
    for (int i = 0; i < List_size(self); i++)
    {
        List *l = List_get(self, i);
        free(l);
    }
    List_deinit(self);
    List_init(self);
    Csv_fillTableFromString(self, str);
    return self;
}

void   List_valueFromListToStr(List * self, char * str, int n)
{
    str[0] = '\0';
    for (int i = 0; i < List_size(self); i++)
    {
        struct Museum *mus = List_get(self, i);
        strcat(str, mus->name);
        strcat(str, ",");
        strcat(str, mus->location);
        strcat(str, ",");
        char  establ[10];
        sprintf(establ, "%d", mus->establ);
        strcat(str, establ);
        strcat(str, ",");
        strcat(str, mus->name);
        strcat(str, "\n");
    }
}

void List_structPrint(List * self)
{
    printf("\n");
    for (int i = 0; i < List_size(self); i++)
    {
        struct Museum * mus = List_get(self, i);
        printf("%s,%s,%d,%s\n", mus->name, mus->location, mus->establ, mus->visitors);
    }
}

void List_clearStruct(List * self)
{
    for (int i = 0; i < List_size(self); i++)
    {
        List * l = List_get(self, i);
        free(l);
    }
}