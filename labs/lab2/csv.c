#include "csv.h"
#include "list.h"

void Csv_addInt(List *row, int value)
{
    const int len = 1;
    char a[len];
    sprintf(a, "%d", value);
    List_add(row, StrOnHeap(a));
}

void Csv_addDouble(List *row, double value)
{
    const int len = 1;
    char a[len];
    sprintf(a, "%.2f", value);
    List_add(row, StrOnHeap(a));
}

void Csv_addString(List *row, const char *value)
{
    char *p = malloc(sizeof(char *) * strlen(value));
    strcpy(p, value);
    List_add(row, StrOnHeap(p));
}

void Csv_addRow(List *table, List *row)
{
    List_add(table, row);
}

List *Csv_row(List *table, int index)
{
    // return table->items[index];
    return List_get(table, index);
}

void Csv_fillTableFromString(List *csvTable, const char *csvString)
{
    char buf[100];
    int bufX = 0;

    List *list1 = NULL;

    while (1)
    {
        if (*csvString == ',')
        {
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }
            buf[bufX] = '\0';
            bufX = 0;
            Csv_addRow(list1, StrOnHeap(&buf[0]));
        }
        else if (*csvString == '\n')
        {
            if (bufX != 0)
            {
                buf[bufX] = '\0';
                bufX = 0;
                Csv_addRow(list1, StrOnHeap(&buf[0]));
                Csv_addRow(csvTable, list1);
            }

            list1 = NULL;
        }
        else if (*csvString == '\0')
        {
            if (bufX == 0)
            {
                break;
            }
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }

            buf[bufX] = '\0';
            Csv_addRow(list1, StrOnHeap(&buf[0]));
            Csv_addRow(csvTable, list1);
            break;
        }
        else
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }
}

int Csv_fillStringFromTable(List *csvTable, char *b, int n)
{
    b[0] = '\0';
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            strcat(b, value);
            if (j != List_size(rowItem) - 1)
            {
                strcat(b, ",");
            }
        }
        if (i != List_size(csvTable) - 1)
        {
            strcat(b, "\n");
        }
    }
    return 0;
}

char *Csv_createStringFromTable(List *csvTable)
{
    char *str = malloc(sizeof(char *) * Csv_sizeOfString(csvTable) - 1); //?????
    str[0] = '\0';

    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            strcat(str, item);
            if (j != List_size(rowItem) - 1)
            {
                strcat(str, ",");
            }
        }
        if (i != List_size(rowItem))
        {
            strcat(str, "\n");
        }
    }
    return str;
}

void Csv_clearTable(List *csvTable)
{
    for (int i = 0; i < List_size(csvTable); i++)
    {
        void *l1 = List_get(csvTable, i);

        for (int j = 0; j < List_size(l1); j++)
        {
            char *str = List_get(l1, j);
            free(str);
        }
        List_free(l1);
    }
    List_free(csvTable);
}

void   Csv_printTable           (List * table, FILE *fp)//
{
    for (int i = 0; i < List_size(table); i++)
    {
        List *l = Csv_row(table, i);
        List_print(l, fp);
    }
}

int Csv_sizeOfString(List *self)
{
    int size = 0;

    for (int i = 0; i < List_size(self); i++)
    {
        List *rowItem = List_get(self, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            while (*item != '\0')
            {
                size += 1;
                item += 1;
            }
        }
    }

    return size;
}

void   Csv_printString          (List * self, int index, FILE * fp)
{
    List *list = List_get(self, index);
    List_print(list, fp);
}